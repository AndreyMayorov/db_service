# DataBase service

**DB-service** - является одним из микросервисов, полученных при разбиении приложения, для работы 
с расписание учебного заведения Франциска Скорины. Данный сервис служит интерфейсом, предоставляющим 
API для работы с базой данных.
    
Далее будут описанны изменения в API, которые произошли при разбиении.


####Полностью удалены следующие endpoints:


- /changes
- /excels
- /schedules
- /static-information
- /users


####Изменены следующие endpoints:

- /academic-degrees - добавлены методы

|название метода|тип запроса|относительный url |возвращаемое значение|аргументы метода|
|---------------|-----------|------------------|---------------------|----------------|
|findAll        |GET        |-                 |List\<AcademicDegree>|Pageable|         
|save           |POST       |-                 |void                 |List\<AcademicDegree>|
|update         |PUT        |-                 |void                 |List\<AcademicDegree>|
|deleteByIds    |DELETE     |-                 |void                 |List\<Long>|
|findById       |GET        |/{id}             |AcademicDegree       |Long|
|findUpdatedRow |GET        |/update           |List\<AcademicDegree>|LocalDateTime|
|findByParameters|GET       |/param-find       |List\<AcademicDegree>|String|

- /chairs - добавлены методы

|название метода|тип запроса|относительный url |возвращаемое значение|аргументы метода|
|---------------|-----------|------------------|---------------------|----------------|
|findAll        |GET        |-                 |List\<Chair>         |Pageable|         
|save           |POST       |-                 |void                 |List\<Chair>|
|update         |PUT        |-                 |void                 |List\<Chair>|
|deleteByIds    |DELETE     |-                 |void                 |List\<Long>|
|findById       |GET        |/{id}             |Chair                |Long|
|findUpdatedRow |GET        |/update           |List\<Chair>         |LocalDateTime|
|findByParameters|GET       |/param-find       |List\<Chair>         |LocalDateTime, String|


- /classrooms - добавлены методы

|название метода|тип запроса|относительный url |возвращаемое значение|аргументы метода|
|---------------|-----------|------------------|---------------------|----------------|
|findById       |GET        |/{id}             |Classroom                |Long|
|findUpdatedRow |GET        |/update           |List\<Classroom>     |LocalDateTime|
|findByParameters|GET       |/param-find       |List\<Classroom>     |Long, String|

- /classrooms - изменения в методах

|название метода|тип запроса|изменение         |
|---------------|-----------|------------------|
|findAll        |GET        |Возвращаемое значение List\<Classroom>, вместо List\<ClassroomDTO>|
|save           |POST       |Метод принимает на вход  List\<Classroom>, вместо List\<ClassroomDTO>|
|update         |PUT        |Метод принимает на вход  List\<Classroom>, вместо List\<ClassroomDTO>|

- /disciplines - добавлены методы

|название метода|тип запроса|относительный url |возвращаемое значение|аргументы метода|
|---------------|-----------|------------------|---------------------|----------------|
|findById       |GET        |/{id}             |Discipline                |Long|
|findUpdatedRow |GET        |/update           |List\<Discipline>    |LocalDateTime|
|findByParameters|GET       |/param-find       |List\<Discipline>    |String|

- /disciplines - изменения в методах

|название метода|тип запроса|изменение         |
|---------------|-----------|------------------|
|findAll        |GET        |Возвращаемое значение List\<Discipline>, вместо List\<DisciplineDTO>|
|save           |POST       |Метод принимает на вход  List\<Discipline>, вместо List\<DisciplineDTO>|
|update         |PUT        |Метод принимает на вход  List\<Discipline>, вместо List\<DisciplineDTO>|

- /events - добавлены методы

|название метода|тип запроса|относительный url |возвращаемое значение|аргументы метода|
|---------------|-----------|------------------|---------------------|----------------|
|findById       |GET        |/{id}             |Event                |Long|
|findByParameters|GET       |/param-find       |List\<Discipline>    |LocalDate, LocalTime, Long, String, String, String|
|findByStudyHourDateBetween|GET| /find-between-date|List\<Event>     |LocalDate,LocalDate|
|findByStudyHourDateAndTimeBetween|GET|/find-between-date-time|List\<Event>|LocalDate, LocalDate, LocalTime, LocalTime|
|findByStudyHourDateBetweenAndTeacherId|GET|/find-between-date-for-teacher|List\<Event>|LocalDate, LocalDate,Long|
|findByStudyHourDateBetweenAndClassroomId|GET|/find-between-date-for-classroom|List\<Event>|LocalDate, LocalDate,Long|
|findByStudyHourDateBetweenAndStudyGroupId|GET|/find-between-date-for-group|List\<Event>|LocalDate, LocalDate,Long|

- /events - изменения в методах

|название метода|тип запроса|изменение         |
|---------------|-----------|------------------|
|findAll        |GET        |Возвращаемое значение List\<Event>, вместо List\<EventDTO>|
|save           |POST       |Метод принимает на вход  List\<Event>, вместо List\<EventDTO>|
|update         |PUT        |Метод принимает на вход  List\<Event>, вместо List\<EventDTO>|  

- /faculties - добавлены методы

|название метода|тип запроса|относительный url |возвращаемое значение|аргументы метода|
|---------------|-----------|------------------|---------------------|----------------|
|findById       |GET        |/{id}             |Faculty              |Long|
|findByParameters|GET       |/param-find       |List\<Faculty>       |String, String, String, Boolean, String|
|findUpdatedRow |GET        |/update           |List\<Faculty>       |LocalDateTime|

- /faculties - изменения в методах

|название метода|тип запроса|изменение         |
|---------------|-----------|------------------|
|findAll        |GET        |Возвращаемое значение List\<Faculty>, вместо List\<FacultyDTO>|
|save           |POST       |Метод принимает на вход  List\<Faculty>, вместо List\<FacultyDTO>|
|update         |PUT        |Метод принимает на вход  List\<Faculty>, вместо List\<FacultyDTO>| 

- /groups - добавлены методы

|название метода|тип запроса|относительный url |возвращаемое значение|аргументы метода|
|---------------|-----------|------------------|---------------------|----------------|
|findById       |GET        |/{id}             |StudyGroup              |Long|
|findByParameters|GET       |/param-find       |List\<StudyGroup>       |String, String, Long, Long|
|findUpdatedRow |GET        |/update           |List\<StudyGroup>       |LocalDateTime|

- /groups - изменения в методах

|название метода|тип запроса|изменение         |
|---------------|-----------|------------------|
|findAll        |GET        |Возвращаемое значение List\<StudyGroup>, вместо List\<StudyGroupDTO>|
|save           |POST       |Метод принимает на вход  List\<StudyGroup>, вместо List\<StudyGroupDTO>|
|update         |PUT        |Метод принимает на вход  List\<StudyGroup>, вместо List\<StudyGroupDTO>|

- /housings - добавлены методы

|название метода|тип запроса|относительный url |возвращаемое значение|аргументы метода|
|---------------|-----------|------------------|---------------------|----------------|
|findById       |GET        |/{id}             |Housing              |Long|
|findByParameters|GET       |/param-find       |List\<Housing>       |String, String, Long, Long|
|findUpdatedRow |GET        |/update           |List\<Housing>       |LocalDateTime|

- /housings - изменения в методах

|название метода|тип запроса|изменение         |
|---------------|-----------|------------------|
|findAll        |GET        |Возвращаемое значение List\<Housing>, вместо List\<HousingDTO>|
|save           |POST       |Метод принимает на вход  List\<Housing>, вместо List\<HousingDTO>|
|update         |PUT        |Метод принимает на вход  List\<Housing>, вместо List\<HousingDTO>|  

- /lessons - добавлены методы

|название метода|тип запроса|относительный url |возвращаемое значение|аргументы метода|
|---------------|-----------|------------------|---------------------|----------------|
|findByStudyHourDateBetween|GET| /find-between-date|List\<Lesson>     |LocalDate,LocalDate|
|findByStudyHourDateAndTimeBetween|GET|/find-between-date-time|List\<Lesson>|LocalDate, LocalDate, LocalTime, LocalTime|
|findByParameters|GET       |/param-find       |List\<Lesson>       |LocalDate, LocalTime, Long, Long|
|findByStudyHourDateBetweenAndTeacherId|GET|/find-between-date-for-teacher|List\<Lesson>|LocalDate, LocalDate,Long|
|findByStudyHourDateBetweenAndClassroomId|GET|/find-between-date-for-classroom|List\<Lesson>|LocalDate, LocalDate,Long|
|findByStudyHourDateBetweenAndStudyGroupId|GET|/find-between-date-for-group|List\<Lesson>|LocalDate, LocalDate,Long|

- /lessons - изменения в методах

|название метода|тип запроса|изменение         |
|---------------|-----------|------------------|
|findAll        |GET        |Возвращаемое значение List\<Lesson>, вместо List\<LessonDTO>|
|save           |POST       |Метод принимает на вход  List\<Lesson>, вместо List\<LessonDTO>|
|update         |PUT        |Метод принимает на вход  List\<Lesson>, вместо List\<LessonDTO>|
|findById       |GET        |Возвращаемое значение Lesson, вместо LessonDTO|

- /lessons - удалены методы

|название метода|тип запроса|url|
|---------------|-----------|---|
|deleteAll      |DELETE     |/delete-all|

- /semester-information - добавлены методы

|название метода|тип запроса|относительный url |возвращаемое значение|аргументы метода|
|---------------|-----------|------------------|---------------------|----------------|
|findById       |GET        |/{id}             |StudyGroupSemesterInformation|Long|
|findByParameters|GET       |/param-find       |List\<StudyGroupSemesterInformation>|LocalDate, LocalDate, Long|

- /teachers - добавлены методы

|название метода|тип запроса|относительный url |возвращаемое значение|аргументы метода|
|---------------|-----------|------------------|---------------------|----------------|
|findById       |GET        |/{id}             |Teacher              |Long|
|findByParameters|GET       |/param-find       |List\<Teacher>       |String, String, String, BigDecimal, String, String, Long, Long, Long, Long|
|findUpdatedRow |GET        |/update           |List\<Teacher>       |LocalDateTime|

- /teachers - изменения в методах

|название метода|тип запроса|изменение         |
|---------------|-----------|------------------|
|findAll        |GET        |Возвращаемое значение List\<Teacher>, вместо List\<TeacherDTO>|
|save           |POST       |Метод принимает на вход  List\<Teacher>, вместо List\<TeacherDTO>|
|update         |PUT        |Метод принимает на вход  List\<Teacher>, вместо List\<TeacherDTO>| 

- /teacher-positions - добавлены методы

|название метода|тип запроса|относительный url |возвращаемое значение|аргументы метода|
|---------------|-----------|------------------|---------------------|----------------|
|findAll        |GET        |-                 |List\<TeacherPosition>|Pageable|         
|save           |POST       |-                 |void                 |List\<TeacherPosition>|
|update         |PUT        |-                 |void                 |List\<TeacherPosition>|
|deleteByIds    |DELETE     |-                 |void                 |List\<Long>|
|findById       |GET        |/{id}             |TeacherPosition       |Long|
|findUpdatedRow |GET        |/update           |List\<TeacherPosition>|LocalDateTime|
|findByParameters|GET       |/param-find       |List\<TeacherPosition>|String|

- /teacher-ranks - добавлены методы

|название метода|тип запроса|относительный url |возвращаемое значение|аргументы метода|
|---------------|-----------|------------------|---------------------|----------------|
|findAll        |GET        |-                 |List\<TeacherRank>|Pageable|         
|save           |POST       |-                 |void                 |List\<TeacherRank>|
|update         |PUT        |-                 |void                 |List\<TeacherRank>|
|deleteByIds    |DELETE     |-                 |void                 |List\<Long>|
|findById       |GET        |/{id}             |TeacherRank       |Long|
|findUpdatedRow |GET        |/update           |List\<TeacherRank>|LocalDateTime|
|findByParameters|GET       |/param-find       |List\<TeacherRank>|String|

*Если интересующий вас метод не был описан выше, значит изменения его не коснулись.