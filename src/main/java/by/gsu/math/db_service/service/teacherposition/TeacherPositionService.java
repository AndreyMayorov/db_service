package by.gsu.math.db_service.service.teacherposition;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.TeacherPosition;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface TeacherPositionService {

    void save(TeacherPosition teacherPosition);

    Optional<TeacherPosition> findById(Long id);

    void deleteById(Long id);

    List<TeacherPosition> findAll(Pageable pageable);

    List<TeacherPosition> findByParam(List<RequestParameter> params);

    List<TeacherPosition> findUpdatedRow(LocalDateTime lastUpdateDate);
}
