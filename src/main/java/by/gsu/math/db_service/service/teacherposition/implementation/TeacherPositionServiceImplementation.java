package by.gsu.math.db_service.service.teacherposition.implementation;

import by.gsu.math.db_service.dao.parameterizedsearch.GenericSearchUtil;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.dao.repository.TeacherPositionRepository;
import by.gsu.math.db_service.model.entity.TeacherPosition;
import by.gsu.math.db_service.service.teacherposition.TeacherPositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class TeacherPositionServiceImplementation implements TeacherPositionService {

    private TeacherPositionRepository teacherPositionRepository;
    private GenericSearchUtil<TeacherPosition> genericSearchUtil;


    @Override
    @Transactional(readOnly = true)
    public List<TeacherPosition> findByParam(List<RequestParameter> params) {
        return genericSearchUtil.findByParam(params, TeacherPosition.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TeacherPosition> findUpdatedRow(final LocalDateTime lastUpdateDate) {
        return teacherPositionRepository.findUpdatedRow(lastUpdateDate);
    }

    @Override
    public void save(final TeacherPosition teacherPosition) {
        teacherPositionRepository.save(teacherPosition);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<TeacherPosition> findById(final Long id) {
        return teacherPositionRepository.findById(id);
    }

    @Override
    public void deleteById(final Long id) {
        teacherPositionRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TeacherPosition> findAll(final Pageable pageable) {
        return teacherPositionRepository.findAll(pageable).getContent();
    }

    @Autowired
    public void setTeacherPositionRepository(final TeacherPositionRepository teacherPositionRepository) {
        this.teacherPositionRepository = teacherPositionRepository;
    }

    @Autowired
    public void setGenericSearchUtil(final GenericSearchUtil<TeacherPosition> genericSearchUtil) {
        this.genericSearchUtil = genericSearchUtil;
    }
}
