package by.gsu.math.db_service.service.group;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.StudyGroup;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface StudyGroupService {

    Optional<StudyGroup> findById(Long id);

    void save(StudyGroup studyGroup);

    void deleteById(Long id);

    List<StudyGroup> findAll(Pageable pageable);

    List<StudyGroup> findByParam(List<RequestParameter> params);

    List<StudyGroup> findUpdatedRow(LocalDateTime lastUpdateDate);
}
