package by.gsu.math.db_service.service.lesson;

import by.gsu.math.db_service.model.entity.*;
import org.springframework.data.jpa.domain.Specification;

import java.sql.Time;
import java.time.LocalDate;
import java.util.List;

public class LessonSpecification {
    public static Specification<Lesson> hasSameStudyHourDate(final LocalDate studyHourDate) {
        return (Specification<Lesson>)
                (root, criteriaQuery, criteriaBuilder)
                        -> criteriaBuilder.equal(root.get("studyHourDate"), studyHourDate);
    }

    public static Specification<Lesson> hasSameStudyHourTime(final Time studyHourTime) {
        return (Specification<Lesson>)
                (root, criteriaQuery, criteriaBuilder)
                        -> criteriaBuilder.equal(root.get("studyHourTime"), studyHourTime);
    }

    public static Specification<Lesson> hasSameGroup(final StudyGroup studyGroup) {
        return (Specification<Lesson>)
                (root, criteriaQuery, criteriaBuilder)
                        -> criteriaBuilder.equal(root.get("group"), studyGroup);
    }


    public static Specification<Lesson> hasSameTeachers(final List<Teacher> teachers) {
        return (Specification<Lesson>)
                (root, criteriaQuery, criteriaBuilder)
                        -> criteriaBuilder.equal(root.get("teachers"), teachers);
    }

    public static Specification<Lesson> hasSameClassrooms(final List<Classroom> classrooms) {
        return (Specification<Lesson>)
                (root, criteriaQuery, criteriaBuilder)
                        -> criteriaBuilder.equal(root.get("classrooms"), classrooms);
    }

    public static Specification<Lesson> hasSameDiscipline(final Discipline discipline) {
        return (Specification<Lesson>)
                (root, criteriaQuery, criteriaBuilder)
                        -> criteriaBuilder.equal(root.get("discipline"), discipline);
    }
}
