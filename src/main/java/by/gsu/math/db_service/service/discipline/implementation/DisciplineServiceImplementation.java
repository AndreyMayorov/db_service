package by.gsu.math.db_service.service.discipline.implementation;

import by.gsu.math.db_service.dao.parameterizedsearch.GenericSearchUtil;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.dao.repository.DisciplineRepository;
import by.gsu.math.db_service.model.entity.Discipline;
import by.gsu.math.db_service.service.discipline.DisciplineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class DisciplineServiceImplementation implements DisciplineService {

    private DisciplineRepository disciplineRepository;
    private GenericSearchUtil<Discipline> genericSearchUtil;

    @Override
    @Transactional(readOnly = true)
    public List<Discipline> findByParam(List<RequestParameter> params) {
        return genericSearchUtil.findByParam(params, Discipline.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Discipline> findUpdatedRow(final LocalDateTime lastUpdateDate) {
        return disciplineRepository.findUpdatedRow(lastUpdateDate);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Discipline> findById(final Long id) {
        return disciplineRepository.findById(id);
    }

    @Override
    public void save(final Discipline discipline) {
        disciplineRepository.save(discipline);
    }

    @Override
    public void deleteById(final Long id) {
        disciplineRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Discipline> findAll(final Pageable pageable) {
        return disciplineRepository.findAll(pageable).getContent();
    }

    @Autowired
    public void setDisciplineRepository(final DisciplineRepository disciplineRepository) {
        this.disciplineRepository = disciplineRepository;
    }

    @Autowired
    public void setGenericSearchUtil(final GenericSearchUtil<Discipline> genericSearchUtil) {
        this.genericSearchUtil = genericSearchUtil;
    }
}
