package by.gsu.math.db_service.service.chair.implementation;

import by.gsu.math.db_service.dao.parameterizedsearch.GenericSearchUtil;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.dao.repository.ChairRepository;
import by.gsu.math.db_service.model.entity.Chair;
import by.gsu.math.db_service.service.chair.ChairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class ChairServiceImplementation implements ChairService {

    private ChairRepository chairRepository;
    private GenericSearchUtil<Chair> genericSearchUtil;

    @Override
    public void save(final Chair chair) {
        chairRepository.save(chair);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Chair> findById(final Long id) {
        return chairRepository.findById(id);
    }

    @Override
    public void deleteById(final Long id) {
        chairRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Chair> findAll(final Pageable pageable) {
        return chairRepository.findAll(pageable).getContent();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Chair> findByParam(List<RequestParameter> params) {
        return genericSearchUtil.findByParam(params, Chair.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Chair> findUpdatedRow(final LocalDateTime lastUpdateDate) {
        return chairRepository.findUpdatedRow(lastUpdateDate);
    }

    @Autowired
    public void setChairRepository(final ChairRepository chairRepository) {
        this.chairRepository = chairRepository;
    }

    @Autowired
    public void setGenericSearchUtil(final GenericSearchUtil<Chair> genericSearchUtil) {
        this.genericSearchUtil = genericSearchUtil;
    }
}
