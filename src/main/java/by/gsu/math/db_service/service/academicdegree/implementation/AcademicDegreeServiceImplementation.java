package by.gsu.math.db_service.service.academicdegree.implementation;

import by.gsu.math.db_service.dao.parameterizedsearch.GenericSearchUtil;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.dao.repository.AcademicDegreeRepository;
import by.gsu.math.db_service.model.entity.AcademicDegree;
import by.gsu.math.db_service.service.academicdegree.AcademicDegreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class AcademicDegreeServiceImplementation implements AcademicDegreeService {

    private AcademicDegreeRepository academicDegreeRepository;
    private GenericSearchUtil<AcademicDegree> genericSearchUtil;

    @Override
    public void save(final AcademicDegree academicDegree) {
        academicDegreeRepository.save(academicDegree);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<AcademicDegree> findById(final Long id) {
        return academicDegreeRepository.findById(id);
    }

    @Override
    public void deleteById(final Long id) {
        academicDegreeRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AcademicDegree> findAll(final Pageable pageable) {
        return academicDegreeRepository.findAll(pageable).getContent();
    }

    @Override
    @Transactional(readOnly = true)
    public List<AcademicDegree> findByParam(final List<RequestParameter> params) {
        return genericSearchUtil.findByParam(params, AcademicDegree.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AcademicDegree> findUpdatedRow(final LocalDateTime lastUpdateDate) {
        return academicDegreeRepository.findUpdatedRow(lastUpdateDate);
    }

    @Autowired
    public void setAcademicDegreeRepository(final AcademicDegreeRepository academicDegreeRepository) {
        this.academicDegreeRepository = academicDegreeRepository;
    }

    @Autowired
    public void setGenericSearchUtil(final GenericSearchUtil<AcademicDegree> genericSearchUtil) {
        this.genericSearchUtil = genericSearchUtil;
    }

}
