package by.gsu.math.db_service.service.teacherrank;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.TeacherRank;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface TeacherRankService {

    void save(TeacherRank teacherRank);

    Optional<TeacherRank> findById(Long id);

    void deleteById(Long id);

    List<TeacherRank> findAll(Pageable pageable);

    List<TeacherRank> findByParam(List<RequestParameter> params);

    List<TeacherRank> findUpdatedRow(LocalDateTime lastUpdateDate);
}
