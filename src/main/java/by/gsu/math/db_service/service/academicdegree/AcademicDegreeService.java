package by.gsu.math.db_service.service.academicdegree;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.AcademicDegree;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface AcademicDegreeService {

    void save(AcademicDegree academicDegree);

    Optional<AcademicDegree> findById(Long id);

    void deleteById(Long id);

    List<AcademicDegree> findAll(Pageable pageable);

    List<AcademicDegree> findByParam(List<RequestParameter> params);

    List<AcademicDegree> findUpdatedRow(LocalDateTime lastUpdateDate);
}
