package by.gsu.math.db_service.service.housing;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Housing;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface HousingService {

    Optional<Housing> findById(Long id);

    void save(Housing housing);

    void deleteById(Long id);

    List<Housing> findAll(Pageable pageable);

    List<Housing> findByParam(List<RequestParameter> params);

    List<Housing> findUpdatedRow(LocalDateTime lastUpdateDate);
}
