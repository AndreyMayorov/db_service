package by.gsu.math.db_service.service.classroom.implementation;

import by.gsu.math.db_service.dao.parameterizedsearch.GenericSearchUtil;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.dao.repository.ClassroomRepository;
import by.gsu.math.db_service.model.entity.Classroom;
import by.gsu.math.db_service.service.classroom.ClassroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class ClassroomServiceImplementation implements ClassroomService {

    private ClassroomRepository classroomRepository;
    private GenericSearchUtil<Classroom> genericSearchUtil;

    @Override
    @Transactional(readOnly = true)
    public Optional<Classroom> findById(final Long id) {
        return classroomRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Classroom> findByClassroomNameAndHousingName(final String classroomName, final String housingName) {
        return classroomRepository.findByClassroomNameAndHousingName(classroomName, housingName);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Classroom> findByParam(List<RequestParameter> params) {
        return genericSearchUtil.findByParam(params, Classroom.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Classroom> findUpdatedRow(final LocalDateTime lastUpdateDate) {
        return classroomRepository.findUpdatedRow(lastUpdateDate);
    }

    @Override
    public void save(final Classroom classroom) {
        classroomRepository.save(classroom);
    }

    @Override
    public void deleteById(final Long id) {
        classroomRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Classroom> findAll(final Pageable pageable) {
        return classroomRepository.findAll(pageable).getContent();
    }

    @Autowired
    public void setClassroomRepository(final ClassroomRepository classroomRepository) {
        this.classroomRepository = classroomRepository;
    }

    @Autowired
    public void setGenericSearchUtil(final GenericSearchUtil<Classroom> genericSearchUtil) {
        this.genericSearchUtil = genericSearchUtil;
    }
}
