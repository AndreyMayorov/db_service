package by.gsu.math.db_service.service.studygroupsemesterinformation.implementation;

import by.gsu.math.db_service.dao.parameterizedsearch.GenericSearchUtil;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.dao.repository.StudyGroupSemesterInformationRepository;
import by.gsu.math.db_service.model.entity.StudyGroupSemesterInformation;
import by.gsu.math.db_service.service.studygroupsemesterinformation.StudyGroupSemesterInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class StudyGroupSemesterInformationServiceImplementation implements StudyGroupSemesterInformationService {

    private StudyGroupSemesterInformationRepository studyGroupSemesterInformationRepository;
    private GenericSearchUtil<StudyGroupSemesterInformation> genericSearchUtil;

    @Override
    @Transactional(readOnly = true)
    public Optional<StudyGroupSemesterInformation> findById(final Long id) {
        return studyGroupSemesterInformationRepository.findById(id);
    }

    @Override
    public void save(final StudyGroupSemesterInformation studyGroupSemesterInformation) {
        studyGroupSemesterInformationRepository.save(studyGroupSemesterInformation);
    }

    @Override
    public void deleteById(final Long id) {
        studyGroupSemesterInformationRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<StudyGroupSemesterInformation> findAll(final Pageable pageable) {
        return studyGroupSemesterInformationRepository.findAll(pageable).getContent();
    }

    @Override
    @Transactional(readOnly = true)
    public List<StudyGroupSemesterInformation> findByParam(List<RequestParameter> params) {
        return genericSearchUtil.findByParam(params, StudyGroupSemesterInformation.class);
    }

    @Autowired
    public void setStudyGroupSemesterInformationRepository(final StudyGroupSemesterInformationRepository studyGroupSemesterInformationRepository) {
        this.studyGroupSemesterInformationRepository = studyGroupSemesterInformationRepository;
    }

    @Autowired
    public void setGenericSearchUtil(final GenericSearchUtil<StudyGroupSemesterInformation> genericSearchUtil) {
        this.genericSearchUtil = genericSearchUtil;
    }
}
