package by.gsu.math.db_service.service.classroom;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Classroom;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ClassroomService {

    Optional<Classroom> findById(Long id);

    void save(Classroom classroom);

    void deleteById(Long id);

    List<Classroom> findAll(Pageable pageable);

    Optional<Classroom> findByClassroomNameAndHousingName(String classroomName, String housingName);

    List<Classroom> findByParam(List<RequestParameter> params);

    List<Classroom> findUpdatedRow(LocalDateTime lastUpdateDate);
}
