package by.gsu.math.db_service.service.teacher.implementation;

import by.gsu.math.db_service.dao.parameterizedsearch.GenericSearchUtil;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.dao.repository.TeacherRepository;
import by.gsu.math.db_service.model.entity.Teacher;
import by.gsu.math.db_service.service.teacher.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class TeacherServiceImplementation implements TeacherService {

    private TeacherRepository teacherRepository;
    private GenericSearchUtil<Teacher> genericSearchUtil;

    @Override
    @Transactional(readOnly = true)
    public Optional<Teacher> findById(final Long id) {
        return teacherRepository.findById(id);
    }

    @Override
    public void save(final Teacher teacher) {
        teacherRepository.save(teacher);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Teacher> findByParam(final List<RequestParameter> params) {
        return genericSearchUtil.findByParam(params, Teacher.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Teacher> findUpdatedRow(final LocalDateTime lastUpdateDate) {
        return teacherRepository.findUpdatedRow(lastUpdateDate);
    }

    @Override
    public void deleteById(final Long id) {
        teacherRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Teacher> findAll(final Pageable pageable) {
        return teacherRepository.findAll(pageable).getContent();
    }

    @Autowired
    public void setTeacherRepository(final TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }

    @Autowired
    public void setGenericSearchUtil(final GenericSearchUtil<Teacher> genericSearchUtil) {
        this.genericSearchUtil = genericSearchUtil;
    }
}
