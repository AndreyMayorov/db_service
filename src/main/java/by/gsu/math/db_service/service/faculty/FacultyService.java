package by.gsu.math.db_service.service.faculty;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Faculty;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface FacultyService {

    Optional<Faculty> findById(Long id);

    void save(Faculty discipline);

    void deleteById(Long id);

    List<Faculty> findAll(Pageable pageable);

    List<Faculty> findByParam(List<RequestParameter> params);

    List<Faculty> findUpdatedRow(LocalDateTime lastUpdateDate);
}
