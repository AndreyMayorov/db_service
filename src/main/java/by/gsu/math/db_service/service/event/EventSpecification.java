package by.gsu.math.db_service.service.event;

import by.gsu.math.db_service.model.entity.Event;
import by.gsu.math.db_service.model.entity.StudyGroup;
import org.springframework.data.jpa.domain.Specification;

import java.sql.Time;
import java.time.LocalDate;

public class EventSpecification {

    public static Specification<Event> hasSameStudyHourDate(final LocalDate studyHourDate) {
        return (Specification<Event>)
                (root, criteriaQuery, criteriaBuilder)
                        -> criteriaBuilder.equal(root.get("studyHourDate"), studyHourDate);
    }

    public static Specification<Event> hasSameStudyHourTime(final Time studyHourTime) {
        return (Specification<Event>)
                (root, criteriaQuery, criteriaBuilder)
                        -> criteriaBuilder.equal(root.get("studyHourTime"), studyHourTime);
    }

    public static Specification<Event> hasSameGroup(final StudyGroup studyGroup) {
        return (Specification<Event>)
                (root, criteriaQuery, criteriaBuilder)
                        -> criteriaBuilder.equal(root.get("group"), studyGroup);
    }

    public static Specification<Event> hasSameDescription(final String description) {
        return (Specification<Event>)
                (root, criteriaQuery, criteriaBuilder)
                        -> criteriaBuilder.equal(root.get("description"), description);
    }

    public static Specification<Event> hasSameSpeaker(final String speaker) {
        return (Specification<Event>)
                (root, criteriaQuery, criteriaBuilder)
                        -> criteriaBuilder.equal(root.get("speaker"), speaker);
    }

    public static Specification<Event> hasSamePlace(final String place) {
        return (Specification<Event>)
                (root, criteriaQuery, criteriaBuilder)
                        -> criteriaBuilder.equal(root.get("place"), place);
    }
}
