package by.gsu.math.db_service.service.teacher;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Teacher;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface TeacherService {

    Optional<Teacher> findById(Long id);

    void save(Teacher teacher);

    void deleteById(Long id);

    List<Teacher> findAll(Pageable pageable);

    List<Teacher> findByParam(List<RequestParameter> parameters);

    List<Teacher> findUpdatedRow(LocalDateTime lastUpdateDate);

}
