package by.gsu.math.db_service.service.group.implementation;

import by.gsu.math.db_service.dao.parameterizedsearch.GenericSearchUtil;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.dao.repository.StudyGroupRepository;
import by.gsu.math.db_service.model.entity.StudyGroup;
import by.gsu.math.db_service.service.group.StudyGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class StudyGroupServiceImplementation implements StudyGroupService {

    private StudyGroupRepository studyGroupRepository;
    private GenericSearchUtil<StudyGroup> genericSearchUtil;


    @Override
    @Transactional(readOnly = true)
    public Optional<StudyGroup> findById(final Long id) {
        return studyGroupRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<StudyGroup> findByParam(List<RequestParameter> params) {
        return genericSearchUtil.findByParam(params, StudyGroup.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<StudyGroup> findUpdatedRow(final LocalDateTime lastUpdateDate) {
        return studyGroupRepository.findUpdatedRow(lastUpdateDate);
    }

    @Override
    public void save(final StudyGroup studyGroup) {
        studyGroupRepository.save(studyGroup);
    }

    @Override
    public void deleteById(final Long id) {
        studyGroupRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<StudyGroup> findAll(final Pageable pageable) {
        return studyGroupRepository.findAll(pageable).getContent();
    }

    @Autowired
    public void setStudyGroupRepository(final StudyGroupRepository studyGroupRepository) {
        this.studyGroupRepository = studyGroupRepository;
    }

    @Autowired
    public void setGenericSearchUtil(final GenericSearchUtil<StudyGroup> genericSearchUtil) {
        this.genericSearchUtil = genericSearchUtil;
    }
}
