package by.gsu.math.db_service.service.lesson.implementation;

import by.gsu.math.db_service.dao.parameterizedsearch.GenericSearchUtil;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.dao.repository.LessonRepository;
import by.gsu.math.db_service.model.entity.Lesson;
import by.gsu.math.db_service.service.event.EventService;
import by.gsu.math.db_service.service.lesson.LessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Service
public class LessonServiceImplementation implements LessonService {

    private LessonRepository lessonRepository;
    private EventService eventService;
    private GenericSearchUtil<Lesson> genericSearchUtil;

    @Override
    @Transactional(readOnly = true)
    public Optional<Lesson> findById(final Long id) {
        return lessonRepository.findById(id);
    }


    @Override
    @Transactional(readOnly = true)
    public List<Lesson> findAll(final Pageable pageable, final Specification<Lesson> lessonSpecification) {
        return lessonRepository.findAll(lessonSpecification, pageable).getContent();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Lesson> findAll(final Pageable pageable) {
        return lessonRepository.findAll(pageable).getContent();
    }

    @Override
    public void save(final Lesson lesson) {
        final Long id = lesson.getId();
        if (id != null && eventService.existsById(id)) {
            eventService.deleteById(id);
            lessonRepository.insert(lesson.getClass().getSimpleName(), lesson.getId(),
                    lesson.getStudyGroup().getId(),
                    lesson.getDiscipline().getId(),
                    lesson.getTeacher().getId(),
                    lesson.getClassroom().getId(),
                    lesson.getStudyHourDate(),
                    lesson.getStudyHourTime(),
                    lesson.getFaculty().getId());
        } else {
            lessonRepository.save(lesson);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Lesson> findByStudyHourDateBetweenAndStudyGroup(final LocalDate beginDate, final LocalDate endDate, final Long groupId) {
        return lessonRepository.findByStudyHourDateBetweenAndStudyGroupId(beginDate, endDate, groupId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Lesson> findByStudyHourDateBetweenAndTeacherId(final LocalDate beginDate, final LocalDate endDate, final Long teacherId) {
        return lessonRepository.findByStudyHourDateBetweenAndTeacherId(beginDate, endDate, teacherId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Lesson> findByStudyHourDateBetweenAndClassroomId(final LocalDate beginDate, final LocalDate endDate, final Long classroomId) {
        return lessonRepository.findByStudyHourDateBetweenAndClassroomId(beginDate, endDate, classroomId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Lesson> findByParam(List<RequestParameter> params) {
        return genericSearchUtil.findByParam(params, Lesson.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Lesson> findByStudyHourDateBetween(final LocalDate startDate, final LocalDate endDate) {
        return lessonRepository.findByStudyHourDateBetween(startDate, endDate);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Lesson> findByStudyHourDateBetweenAndStudyHourTimeBetween(final LocalDate startDate, final LocalDate endDate, final LocalTime startTime, final LocalTime endTime) {
        return lessonRepository.findByStudyHourDateBetweenAndStudyHourTimeBetween(startDate, endDate, startTime, endTime);
    }

    @Override
    @Transactional
    public void templateSave(final List<List<Lesson>> schedule) {
        for (List<Lesson> lessons : schedule) {
            for (Lesson lesson : lessons) {
                lessonRepository.save(lesson);
            }
        }
    }

    @Override
    public boolean existsById(final Long id) {
        return lessonRepository.existsById(id);
    }

    @Override
    public void deleteById(final Long lessonId) {
        lessonRepository.deleteById(lessonId);
    }

    @Autowired
    public void setLessonRepository(final LessonRepository lessonRepository) {
        this.lessonRepository = lessonRepository;
    }

    @Autowired
    public void setEventService(final EventService eventService) {
        this.eventService = eventService;
    }

    @Autowired
    public void setGenericSearchUtil(final GenericSearchUtil<Lesson> genericSearchUtil) {
        this.genericSearchUtil = genericSearchUtil;
    }
}
