package by.gsu.math.db_service.service.faculty.implementation;

import by.gsu.math.db_service.dao.parameterizedsearch.GenericSearchUtil;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.dao.repository.FacultyRepository;
import by.gsu.math.db_service.model.entity.Faculty;
import by.gsu.math.db_service.service.faculty.FacultyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class FacultyServiceImplementation implements FacultyService {

    private FacultyRepository facultyRepository;
    private GenericSearchUtil<Faculty> genericSearchUtil;

    @Override
    @Transactional(readOnly = true)
    public List<Faculty> findByParam(List<RequestParameter> params) {
        return genericSearchUtil.findByParam(params, Faculty.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Faculty> findUpdatedRow(final LocalDateTime lastUpdateDate) {
        return facultyRepository.findUpdatedRow(lastUpdateDate);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Faculty> findById(final Long id) {
        return facultyRepository.findById(id);
    }

    @Override
    public void save(final Faculty faculty) {
        facultyRepository.save(faculty);
    }

    @Override
    public void deleteById(final Long id) {
        facultyRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Faculty> findAll(final Pageable pageable) {
        return facultyRepository.findAll(isDisplayed(true), pageable).getContent();
    }

    private Specification<Faculty> isDisplayed(final Boolean displayed) {
        return (root, criteriaQuery, criteriaBuilder)
                -> criteriaBuilder.equal(root.get("displayed"), displayed);
    }

    @Autowired
    public void setFacultyRepository(final FacultyRepository facultyRepository) {
        this.facultyRepository = facultyRepository;
    }

    @Autowired
    public void setGenericSearchUtil(final GenericSearchUtil<Faculty> genericSearchUtil) {
        this.genericSearchUtil = genericSearchUtil;
    }
}
