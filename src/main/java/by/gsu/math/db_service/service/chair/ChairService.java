package by.gsu.math.db_service.service.chair;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Chair;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ChairService {

    void save(Chair chair);

    Optional<Chair> findById(Long id);

    void deleteById(Long id);

    List<Chair> findAll(Pageable pageable);

    List<Chair> findByParam(List<RequestParameter> params);

    List<Chair> findUpdatedRow(LocalDateTime lastUpdateDate);
}
