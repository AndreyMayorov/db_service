package by.gsu.math.db_service.service.studygroupsemesterinformation;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.StudyGroupSemesterInformation;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface StudyGroupSemesterInformationService {

    Optional<StudyGroupSemesterInformation> findById(Long id);

    void save(StudyGroupSemesterInformation studyGroupSemesterInformation);

    void deleteById(Long id);

    List<StudyGroupSemesterInformation> findAll(Pageable pageable);

    List<StudyGroupSemesterInformation> findByParam(List<RequestParameter> params);
}
