package by.gsu.math.db_service.service.housing.implementation;

import by.gsu.math.db_service.dao.parameterizedsearch.GenericSearchUtil;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.dao.repository.HousingRepository;
import by.gsu.math.db_service.model.entity.Housing;
import by.gsu.math.db_service.service.housing.HousingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class HousingServiceImplementation implements HousingService {

    private HousingRepository housingRepository;
    private GenericSearchUtil<Housing> genericSearchUtil;

    @Override
    @Transactional(readOnly = true)
    public Optional<Housing> findById(final Long id) {
        return housingRepository.findById(id);
    }

    @Override
    public void save(final Housing housing) {
        housingRepository.save(housing);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Housing> findByParam(List<RequestParameter> params) {
        return genericSearchUtil.findByParam(params, Housing.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Housing> findUpdatedRow(final LocalDateTime lastUpdateDate) {
        return housingRepository.findUpdatedRow(lastUpdateDate);
    }

    @Override
    public void deleteById(final Long id) {
        housingRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Housing> findAll(final Pageable pageable) {
        return housingRepository.findAll(pageable).getContent();
    }

    @Autowired
    public void setHousingRepository(final HousingRepository housingRepository) {
        this.housingRepository = housingRepository;
    }

    @Autowired
    public void setGenericSearchUtil(final GenericSearchUtil<Housing> genericSearchUtil) {
        this.genericSearchUtil = genericSearchUtil;
    }
}
