package by.gsu.math.db_service.service.event.implementation;

import by.gsu.math.db_service.dao.parameterizedsearch.GenericSearchUtil;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.dao.repository.EventRepository;
import by.gsu.math.db_service.model.entity.Event;
import by.gsu.math.db_service.service.event.EventService;
import by.gsu.math.db_service.service.lesson.LessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Service
public class EventServiceImplementation implements EventService {

    private EventRepository eventRepository;
    private GenericSearchUtil<Event> genericSearchUtil;
    private LessonService lessonService;

    @Autowired
    public void setLessonService(final LessonService lessonService) {
        this.lessonService = lessonService;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Event> findById(final Long id) {
        return eventRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Event> findAll(final Pageable pageable, final Specification<Event> eventSpecification) {
        return eventRepository.findAll(eventSpecification, pageable).getContent();
    }

    @Override
    @Transactional
    public void save(final Event event) {
        final Long id = event.getId();
        if (id != null && lessonService.existsById(id)) {
            lessonService.deleteById(id);
            eventRepository.insert(event.getClass().getSimpleName(), id,
                    event.getStudyGroup().getId(), event.getStudyHourDate(),
                    event.getStudyHourTime(), event.getDescription(),
                    event.getSpeaker(), event.getPlace(), event.getFaculty().getId());
        } else {
            eventRepository.save(event);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Event> findByStudyHourDateBetweenAndTeacherId(final LocalDate beginDate, final LocalDate endDate, final Long teacherId) {
        return eventRepository.findByStudyHourDateBetweenAndTeacherId(beginDate, endDate, teacherId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Event> findByStudyHourDateBetweenAndClassroomId(final LocalDate beginDate, final LocalDate endDate, final Long classroomId) {
        return eventRepository.findByStudyHourDateBetweenAndClassroomId(beginDate, endDate, classroomId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Event> findByParam(List<RequestParameter> params) {
        return genericSearchUtil.findByParam(params, Event.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Event> findByStudyHourDateBetween(final LocalDate startDate, final LocalDate endDate) {
        return eventRepository.findByStudyHourDateBetween(startDate, endDate);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Event> findByStudyHourDateBetweenAndStudyHourTimeBetween(final LocalDate startDate, final LocalDate endDate,
                                                                         final LocalTime startTime, final LocalTime endTime) {
        return eventRepository.findByStudyHourDateBetweenAndStudyHourTimeBetween(startDate, endDate, startTime, endTime);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Event> findByStudyHourDateBetweenAndStudyGroup(final LocalDate beginDate, final LocalDate endDate, final Long groupId) {
        return eventRepository.findByStudyHourDateBetweenAndStudyGroupId(beginDate, endDate, groupId);
    }

    @Override
    public boolean existsById(final Long id) {
        return eventRepository.existsById(id);
    }

    @Override
    public void deleteById(final Long id) {
        eventRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Event> findAll(final Pageable pageable) {
        return eventRepository.findAll(pageable).getContent();
    }

    @Autowired
    public void setEventRepository(final EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    @Autowired
    public void setGenericSearchUtil(final GenericSearchUtil<Event> genericSearchUtil) {
        this.genericSearchUtil = genericSearchUtil;
    }
}
