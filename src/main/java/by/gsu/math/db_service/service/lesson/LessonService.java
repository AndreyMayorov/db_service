package by.gsu.math.db_service.service.lesson;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Lesson;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

public interface LessonService {

    Optional<Lesson> findById(Long id);

    List<Lesson> findAll(Pageable pageable, Specification<Lesson> lessonSpecification);

    void templateSave(List<List<Lesson>> schedule);

    void deleteById(Long lessonId);

    List<Lesson> findAll(Pageable pageable);

    boolean existsById(Long id);

    void save(Lesson lesson);

    List<Lesson> findByStudyHourDateBetweenAndStudyGroup(LocalDate beginDate, LocalDate endDate, Long groupId);

    List<Lesson> findByStudyHourDateBetweenAndTeacherId(LocalDate beginDate, LocalDate endDate, Long teacherId);

    List<Lesson> findByStudyHourDateBetweenAndClassroomId(LocalDate beginDate, LocalDate endDate, Long classroomId);

    List<Lesson> findByParam(List<RequestParameter> params);

    List<Lesson> findByStudyHourDateBetween(LocalDate startDate, LocalDate endDate);

    List<Lesson> findByStudyHourDateBetweenAndStudyHourTimeBetween(LocalDate startDate, LocalDate endDate, LocalTime startTime, LocalTime endTime);
}
