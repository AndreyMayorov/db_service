package by.gsu.math.db_service.service.teacherrank.implementation;

import by.gsu.math.db_service.dao.parameterizedsearch.GenericSearchUtil;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.dao.repository.TeacherRankRepository;
import by.gsu.math.db_service.model.entity.TeacherRank;
import by.gsu.math.db_service.service.teacherrank.TeacherRankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class TeacherRankServiceImplementation implements TeacherRankService {

    private TeacherRankRepository teacherRankRepository;
    private GenericSearchUtil<TeacherRank> genericSearchUtil;

    @Override
    @Transactional(readOnly = true)
    public List<TeacherRank> findByParam(List<RequestParameter> params) {
        return genericSearchUtil.findByParam(params, TeacherRank.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TeacherRank> findUpdatedRow(final LocalDateTime lastUpdateDate) {
        return teacherRankRepository.findUpdatedRow(lastUpdateDate);
    }

    @Override
    public void save(final TeacherRank teacherRank) {
        teacherRankRepository.save(teacherRank);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<TeacherRank> findById(final Long id) {
        return teacherRankRepository.findById(id);
    }

    @Override
    public void deleteById(final Long id) {
        teacherRankRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TeacherRank> findAll(final Pageable pageable) {
        return teacherRankRepository.findAll(pageable).getContent();
    }

    @Autowired
    public void setTeacherRankRepository(final TeacherRankRepository teacherRankRepository) {
        this.teacherRankRepository = teacherRankRepository;
    }

    @Autowired
    public void setGenericSearchUtil(final GenericSearchUtil<TeacherRank> genericSearchUtil) {
        this.genericSearchUtil = genericSearchUtil;
    }
}
