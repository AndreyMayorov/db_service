package by.gsu.math.db_service.service.event;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Event;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

public interface EventService {

    Optional<Event> findById(Long id);

    List<Event> findAll(Pageable pageable, Specification<Event> eventSpecification);

    void save(Event event);

    void deleteById(Long id);

    List<Event> findAll(Pageable pageable);

    boolean existsById(Long id);

    List<Event> findByStudyHourDateBetweenAndStudyGroup(LocalDate beginDate, LocalDate endDate, Long groupId);

    List<Event> findByStudyHourDateBetweenAndTeacherId(LocalDate beginDate, LocalDate endDate, Long teacherId);

    List<Event> findByStudyHourDateBetweenAndClassroomId(LocalDate beginDate, LocalDate endDate, Long classroomId);

    List<Event> findByParam(List<RequestParameter> params);

    List<Event> findByStudyHourDateBetween(LocalDate startDate, LocalDate endDate);

    List<Event> findByStudyHourDateBetweenAndStudyHourTimeBetween(LocalDate startDate, LocalDate endDate, LocalTime startTime, LocalTime endTime);
}
