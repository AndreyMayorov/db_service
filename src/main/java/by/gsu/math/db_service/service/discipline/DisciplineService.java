package by.gsu.math.db_service.service.discipline;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Discipline;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface DisciplineService {

    Optional<Discipline> findById(Long id);

    void save(Discipline discipline);

    void deleteById(Long id);

    List<Discipline> findAll(Pageable pageable);

    List<Discipline> findByParam(List<RequestParameter> params);

    List<Discipline> findUpdatedRow(LocalDateTime lastUpdateDate);
}
