package by.gsu.math.db_service.configuration;

import by.gsu.math.db_service.dao.parameterizedsearch.ParametrizedSearchAnnotationAnalyser;
import by.gsu.math.db_service.web.filter.ParametrizedSearchRequestConverterFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CharacterEncodingFilter;

@Configuration
public class FilterConfiguration {

    private ParametrizedSearchAnnotationAnalyser parametrizedSearchAnnotationAnalyser;

    private ParametrizedSearchRequestConverterFilter parametrizedSearchRequestConverterFilter;

    @Bean
    public FilterRegistrationBean<CharacterEncodingFilter> characterEncodingFilter() {
        FilterRegistrationBean<CharacterEncodingFilter> registration = new FilterRegistrationBean<>(new CharacterEncodingFilter());
        registration.setOrder(Integer.MAX_VALUE - 1);
        registration.setName("CharacterEncodingFilter");
        return registration;
    }

    @Bean
    public FilterRegistrationBean<ParametrizedSearchRequestConverterFilter> loggingFilter() {
        FilterRegistrationBean<ParametrizedSearchRequestConverterFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(parametrizedSearchRequestConverterFilter);
        registrationBean.addUrlPatterns(parametrizedSearchAnnotationAnalyser.getParamFindControllerURLs().toArray(new String[0]));
        return registrationBean;
    }

    @Autowired
    public void setParametrizedSearchAnnotationAnalyser(ParametrizedSearchAnnotationAnalyser parametrizedSearchAnnotationAnalyser) {
        this.parametrizedSearchAnnotationAnalyser = parametrizedSearchAnnotationAnalyser;
    }

    @Autowired
    public void setParametrizedSearchRequestConverterFilter(ParametrizedSearchRequestConverterFilter parametrizedSearchRequestConverterFilter) {
        this.parametrizedSearchRequestConverterFilter = parametrizedSearchRequestConverterFilter;
    }
}
