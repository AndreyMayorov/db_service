package by.gsu.math.db_service.web.controller;

import by.gsu.math.db_service.Constant;
import by.gsu.math.db_service.dao.parameterizedsearch.ParametrizedSearchMethod;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.exception.NotFoundException;
import by.gsu.math.db_service.model.entity.StudyGroupSemesterInformation;
import by.gsu.math.db_service.service.studygroupsemesterinformation.StudyGroupSemesterInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/semester-information")
public class StudyGroupSemesterInformationController {

    private StudyGroupSemesterInformationService studyGroupSemesterInformationService;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    List<StudyGroupSemesterInformation> findAll(final Pageable pageable) {
        return studyGroupSemesterInformationService.findAll(pageable);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void save(final List<StudyGroupSemesterInformation> studyGroupsSemesterInformation) {
        studyGroupsSemesterInformation.forEach(
                studyGroupSemesterInformationService::save);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void update(final List<StudyGroupSemesterInformation> studyGroupsSemesterInformation) {
        studyGroupsSemesterInformation.forEach(
                studyGroupSemesterInformationService::save);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void deleteById(@RequestBody final List<Long> ids) {
        ids.forEach(studyGroupSemesterInformationService::deleteById);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    StudyGroupSemesterInformation findById(@PathVariable @NotNull final Long id) {
        return studyGroupSemesterInformationService.findById(id).orElseThrow(()
                -> new NotFoundException(Constant.NOT_FOUND_RESOURCE_MESSAGE));
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @ParametrizedSearchMethod(entityClass = StudyGroupSemesterInformation.class)
    @GetMapping(value = "/param-find", produces = MediaType.APPLICATION_JSON_VALUE)
    List<StudyGroupSemesterInformation> findByParameters(@RequestAttribute(value = "requestParameters") List<RequestParameter> params) {
        return studyGroupSemesterInformationService.findByParam(params);
    }

    @Autowired
    public void setStudyGroupSemesterInformationService(final StudyGroupSemesterInformationService studyGroupSemesterInformationService) {
        this.studyGroupSemesterInformationService = studyGroupSemesterInformationService;
    }
}
