package by.gsu.math.db_service.web.controller.handler;

import by.gsu.math.db_service.exception.BadRequestException;
import by.gsu.math.db_service.exception.NotFoundException;
import org.hibernate.TransientPropertyValueException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

/**
 * This is handler class for handle some exceptions and return associated status code to the client
 */
@ControllerAdvice
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * This method handles exceptions and returns 500 status code with message of cause
     * @param exception - initiated exception
     * @param request - request from client
     * @return ResponseEntity with error details and http status code
     */
        /*@ExceptionHandler(Exception.class)
        public final ResponseEntity<Object> handleAllExceptions(final Exception exception, final WebRequest request) {
            ErrorDetails errorDetails = new ErrorDetails(HttpStatus.INTERNAL_SERVER_ERROR, new Date(), exception.getMessage(),
                    request.getDescription(false));
            return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
        }*/

    /**
     * This method handles NotFoundException and returns 401 status code with message of cause
     *
     * @param exception - NotFoundException
     * @param request   - request from client
     * @return ResponseEntity with error details and http status code
     */
    @ExceptionHandler({NotFoundException.class})
    public final ResponseEntity<Object> handleNotFoundException(final NotFoundException exception,
                                                                final WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails(HttpStatus.NOT_FOUND, new Date(), exception.getMessage(),
                request.getDescription(false));
        return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException exception,
                                                                  final HttpHeaders headers,
                                                                  final HttpStatus status,
                                                                  final WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails(HttpStatus.BAD_REQUEST, new Date(), "Validation Failed",
                exception.getBindingResult().toString());
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({TransientPropertyValueException.class, BadRequestException.class})
    public final ResponseEntity<Object> handleBadRequestException(final RuntimeException exception,
                                                                  final WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails(HttpStatus.BAD_REQUEST, new Date(), exception.getMessage(),
                request.getDescription(false));
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }
}
