package by.gsu.math.db_service.web.controller.handler;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Date;

@Data
class ErrorDetails {

    private HttpStatus httpStatus;
    private Date timestamp;
    private String message;
    private String details;

    ErrorDetails(final HttpStatus httpStatus, final Date timestamp, final String message, final String details) {
        this.timestamp = timestamp;
        this.message = message;
        this.details = details;
        this.httpStatus = httpStatus;
    }
}
