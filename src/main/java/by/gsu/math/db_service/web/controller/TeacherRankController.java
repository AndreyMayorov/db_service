package by.gsu.math.db_service.web.controller;

import by.gsu.math.db_service.dao.parameterizedsearch.ParametrizedSearchMethod;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.TeacherRank;
import by.gsu.math.db_service.model.facade.teacherrank.TeacherRankFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/teacher-ranks")
public class TeacherRankController {

    private TeacherRankFacade teacherRankFacade;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    List<TeacherRank> findAll(final Pageable pageable) {
        return teacherRankFacade.findAll(pageable);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    TeacherRank findById(@PathVariable @NotNull final Long id) {
        return teacherRankFacade.findById(id);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void save(@RequestBody final List<TeacherRank> teachers) {
        teachers.forEach(teacherRankFacade::save);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void update(@RequestBody final List<TeacherRank> teachers) {
        teachers.forEach(teacherRankFacade::save);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void deleteByIds(@RequestBody final List<Long> ids) {
        ids.forEach(teacherRankFacade::deleteById);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @ParametrizedSearchMethod(entityClass = TeacherRank.class)
    @GetMapping(value = "/param-find", produces = MediaType.APPLICATION_JSON_VALUE)
    List<TeacherRank> findByParameters(@RequestAttribute(value = "requestParameters") List<RequestParameter> params) {
        return teacherRankFacade.findByParam(params);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    List<TeacherRank> findUpdatedRow(@RequestParam(value = "date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final LocalDateTime date) {
        return teacherRankFacade.findUpdatedRow(date);
    }

    @Autowired
    public void setTeacherRankFacade(final TeacherRankFacade teacherRankFacade) {
        this.teacherRankFacade = teacherRankFacade;
    }
}
