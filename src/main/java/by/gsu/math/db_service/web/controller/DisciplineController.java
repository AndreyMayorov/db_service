package by.gsu.math.db_service.web.controller;

import by.gsu.math.db_service.dao.parameterizedsearch.ParametrizedSearchMethod;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Discipline;
import by.gsu.math.db_service.model.facade.discipline.DisciplineFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/disciplines")
public class DisciplineController {

    private DisciplineFacade disciplineFacade;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    List<Discipline> findAll(final Pageable pageable) {
        return disciplineFacade.findAll(pageable);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    Discipline findById(@PathVariable @NotNull final Long id) {
        return disciplineFacade.findById(id);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void save(@RequestBody final List<Discipline> disciplines) {
        disciplines.forEach(disciplineFacade::save);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void update(@RequestBody final List<Discipline> disciplines) {
        disciplines.forEach(disciplineFacade::save);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void deleteByIds(@RequestBody final List<Long> ids) {
        ids.forEach(disciplineFacade::deleteById);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @ParametrizedSearchMethod(entityClass = Discipline.class)
    @GetMapping(value = "/param-find", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Discipline> findByParameters(@RequestAttribute(value = "requestParameters") List<RequestParameter> params) {
        return disciplineFacade.findByParam(params);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Discipline> findUpdatedRow(@RequestParam(value = "date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final LocalDateTime date) {
        return disciplineFacade.findUpdatedRow(date);
    }

    @Autowired
    public void setDisciplineFacade(final DisciplineFacade disciplineFacade) {
        this.disciplineFacade = disciplineFacade;
    }
}
