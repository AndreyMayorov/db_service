package by.gsu.math.db_service.web.controller;

import by.gsu.math.db_service.dao.parameterizedsearch.ParametrizedSearchMethod;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Event;
import by.gsu.math.db_service.model.facade.event.EventFacade;
import by.gsu.math.db_service.utils.DateChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@RestController
@RequestMapping("/events")
public class EventController {

    private EventFacade eventFacade;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    List<Event> findAll(final Pageable pageable) {
        return eventFacade.findAll(pageable);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    Event findById(@PathVariable @NotNull final Long id) {
        return eventFacade.findById(id);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void save(@RequestBody final List<Event> events) {
        events.forEach(eventFacade::save);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void update(@RequestBody final List<Event> events) {
        events.forEach(eventFacade::save);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void deleteByIds(@RequestBody final List<Long> ids) {
        ids.forEach(eventFacade::deleteById);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @ParametrizedSearchMethod(entityClass = Event.class)
    @GetMapping(value = "/param-find", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Event> findByParameters(@RequestAttribute(value = "requestParameters") List<RequestParameter> params) {
        return eventFacade.findByParam(params);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/find-between-date", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Event> findByStudyHourDateBetween(@RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate startDate,
                                           @RequestParam(value = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate endDate) {
        DateChecker.validateDatePeriod(startDate, endDate);
        return eventFacade.findByStudyHourDateBetween(startDate, endDate);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/find-between-date-time", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Event> findByStudyHourDateAndTimeBetween(
            @RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate startDate,
            @RequestParam(value = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate endDate,
            @RequestParam(value = "startTime") @DateTimeFormat(iso = DateTimeFormat.ISO.TIME) final LocalTime startTime,
            @RequestParam(value = "endTime") @DateTimeFormat(iso = DateTimeFormat.ISO.TIME) final LocalTime endTime) {
        DateChecker.validateDatePeriod(startDate, endDate);
        return eventFacade.findByStudyHourDateBetweenAndStudyHourTimeBetween(startDate, endDate, startTime, endTime);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/find-between-date-for-group", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Event> findByStudyHourDateBetweenAndStudyGroupId(
            @RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate startDate,
            @RequestParam(value = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate endDate,
            @RequestParam(value = "groupId") final Long groupId) {
        DateChecker.validateDatePeriod(startDate, endDate);
        return eventFacade.findByStudyHourDateBetweenAndStudyGroupId(startDate, endDate, groupId);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/find-between-date-for-teacher", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Event> findByStudyHourDateBetweenAndTeacherId(
            @RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate startDate,
            @RequestParam(value = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate endDate,
            @RequestParam(value = "teacherId") final Long teacherId) {
        DateChecker.validateDatePeriod(startDate, endDate);
        return eventFacade.findByStudyHourDateBetweenAndTeacherId(startDate, endDate, teacherId);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/find-between-date-for-classroom", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Event> findByStudyHourDateBetweenAndClassroomId(
            @RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate startDate,
            @RequestParam(value = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate endDate,
            @RequestParam(value = "classroomId") final Long classroomId) {
        DateChecker.validateDatePeriod(startDate, endDate);
        return eventFacade.findByStudyHourDateBetweenAndClassroomId(startDate, endDate, classroomId);
    }


    @Autowired
    public void setEventFacade(final EventFacade eventFacade) {
        this.eventFacade = eventFacade;
    }
}
