package by.gsu.math.db_service.web.controller;

import by.gsu.math.db_service.dao.parameterizedsearch.ParametrizedSearchMethod;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Chair;
import by.gsu.math.db_service.model.facade.chair.ChairFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/chairs")
public class ChairController {

    private ChairFacade chairFacade;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    List<Chair> findAll(final Pageable pageable) {
        return chairFacade.findAll(pageable);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void save(@RequestBody final List<Chair> chairs) {
        chairs.forEach(chairFacade::save);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void update(@RequestBody final List<Chair> chairs) {
        chairs.forEach(chairFacade::save);
    }

    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deleteByIds(@RequestBody final List<Long> ids) {
        ids.forEach(chairFacade::deleteById);
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    Chair findById(@PathVariable @NotNull final Long id) {
        return chairFacade.findById(id);
    }

    @GetMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    List<Chair> findUpdatedRow(@RequestParam(value = "date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final LocalDateTime date) {
        return chairFacade.findUpdatedRow(date);
    }

    @ParametrizedSearchMethod(entityClass = Chair.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/param-find", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Chair> findByParameters(@RequestAttribute(value = "requestParameters") List<RequestParameter> params) {
        return chairFacade.findByParam(params);
    }

    @Autowired
    public void setTeacherFacade(final ChairFacade chairFacade) {
        this.chairFacade = chairFacade;
    }


}
