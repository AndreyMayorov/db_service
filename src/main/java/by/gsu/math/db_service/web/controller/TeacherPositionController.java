package by.gsu.math.db_service.web.controller;

import by.gsu.math.db_service.dao.parameterizedsearch.ParametrizedSearchMethod;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.TeacherPosition;
import by.gsu.math.db_service.model.facade.teacherposition.TeacherPositionFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/teacher-positions")
public class TeacherPositionController {

    private TeacherPositionFacade teacherPositionFacade;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    TeacherPosition findById(@PathVariable @NotNull final Long id) {
        return teacherPositionFacade.findById(id);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    List<TeacherPosition> findAll(final Pageable pageable) {
        return teacherPositionFacade.findAll(pageable);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void save(@RequestBody final List<TeacherPosition> teacherPositions) {
        teacherPositions.forEach(teacherPositionFacade::save);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void update(@RequestBody final List<TeacherPosition> teacherPositions) {
        teacherPositions.forEach(teacherPositionFacade::save);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void deleteByIds(@RequestBody final List<Long> ids) {
        ids.forEach(teacherPositionFacade::deleteById);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @ParametrizedSearchMethod(entityClass = TeacherPosition.class)
    @GetMapping(value = "/param-find", produces = MediaType.APPLICATION_JSON_VALUE)
    List<TeacherPosition> findByParameters(@RequestAttribute(value = "requestParameters") List<RequestParameter> params) {
        return teacherPositionFacade.findByParam(params);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    List<TeacherPosition> findUpdatedRow(@RequestParam(value = "date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final LocalDateTime date) {
        return teacherPositionFacade.findUpdatedRow(date);
    }

    @Autowired
    public void setTeacherPositionFacade(final TeacherPositionFacade teacherPositionFacade) {
        this.teacherPositionFacade = teacherPositionFacade;
    }
}
