package by.gsu.math.db_service.web.controller;

import by.gsu.math.db_service.dao.parameterizedsearch.ParametrizedSearchMethod;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Lesson;
import by.gsu.math.db_service.model.facade.lesson.LessonFacade;
import by.gsu.math.db_service.utils.DateChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@RestController
@RequestMapping("/lessons")
public class LessonController {

    private LessonFacade lessonFacade;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    List<Lesson> findAll(final Pageable pageable) {
        return lessonFacade.findAll(pageable);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    Lesson findById(@PathVariable @NotNull final Long id) {
        return lessonFacade.findById(id);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void save(@RequestBody final List<Lesson> lessons) {
        lessons.forEach(lessonFacade::save);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void update(@RequestBody final List<Lesson> lessons) {
        lessons.forEach(lessonFacade::save);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void deleteByIds(@RequestBody final List<Long> ids) {
        ids.forEach(lessonFacade::deleteById);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping(path = "/swap/{firstLessonId}/{secondLessonId}", produces = MediaType.APPLICATION_JSON_VALUE)
    void swap(@PathVariable final Long firstLessonId, @PathVariable final Long secondLessonId) {
        lessonFacade.swap(firstLessonId, secondLessonId);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/{date}/{time}/{lessonId}", produces = MediaType.APPLICATION_JSON_VALUE)
    void setDateAndTime(@PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate date,
                        @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.TIME) final LocalTime time,
                        @PathVariable final Long lessonId) {
        lessonFacade.setDateAndTime(date, time, lessonId);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/find-between-date", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Lesson> findByStudyHourDateBetween(@RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate startDate,
                                            @RequestParam(value = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate endDate) {
        DateChecker.validateDatePeriod(startDate, endDate);
        return lessonFacade.findByStudyHourDateBetween(startDate, endDate);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/find-between-date-time", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Lesson> findByStudyHourDateAndTimeBetween(@RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate startDate,
                                                   @RequestParam(value = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate endDate,
                                                   @RequestParam(value = "startTime") @DateTimeFormat(iso = DateTimeFormat.ISO.TIME) final LocalTime startTime,
                                                   @RequestParam(value = "endTime") @DateTimeFormat(iso = DateTimeFormat.ISO.TIME) final LocalTime endTime) {
        DateChecker.validateDatePeriod(startDate, endDate);
        return lessonFacade.findByStudyHourDateBetweenAndStudyHourTimeBetween(startDate, endDate, startTime, endTime);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/find-between-date-for-group", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Lesson> findByStudyHourDateBetweenAndStudyGroupId(@RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate startDate,
                                                           @RequestParam(value = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate endDate,
                                                           @RequestParam(value = "groupId") final Long groupId) {
        DateChecker.validateDatePeriod(startDate, endDate);
        return lessonFacade.findByStudyHourDateBetweenAndStudyGroupId(startDate, endDate, groupId);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/find-between-date-for-teacher", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Lesson> findByStudyHourDateBetweenAndTeacherId(@RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate startDate,
                                                        @RequestParam(value = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate endDate,
                                                        @RequestParam(value = "teacherId") final Long teacherId) {
        DateChecker.validateDatePeriod(startDate, endDate);
        return lessonFacade.findByStudyHourDateBetweenAndTeacherId(startDate, endDate, teacherId);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/find-between-date-for-classroom", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Lesson> findByStudyHourDateBetweenAndClassroomId(@RequestParam(value = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate startDate,
                                                          @RequestParam(value = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate endDate,
                                                          @RequestParam(value = "classroomId") final Long classroomId) {
        DateChecker.validateDatePeriod(startDate, endDate);
        return lessonFacade.findByStudyHourDateBetweenAndClassroomId(startDate, endDate, classroomId);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @ParametrizedSearchMethod(entityClass = Lesson.class)
    @GetMapping(value = "/param-find", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Lesson> findByParameters(@RequestAttribute(value = "requestParameters") List<RequestParameter> params) {
        return lessonFacade.findByParam(params);
    }

    @Autowired
    public void setLessonFacade(final LessonFacade lessonFacade) {
        this.lessonFacade = lessonFacade;
    }
}

