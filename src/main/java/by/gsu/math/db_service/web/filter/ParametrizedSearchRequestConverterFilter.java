package by.gsu.math.db_service.web.filter;

import by.gsu.math.db_service.dao.parameterizedsearch.ParametrizedSearchAnnotationAnalyser;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.exception.BadRequestException;
import org.apache.catalina.connector.RequestFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class ParametrizedSearchRequestConverterFilter implements Filter {

    private Map<String, Map<String, Class<?>>> entitiesFieldsMap;

    private ParametrizedSearchAnnotationAnalyser parametrizedSearchAnnotationAnalyser;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        entitiesFieldsMap = parametrizedSearchAnnotationAnalyser.getEntitiesFieldsMap();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String entityMarker = getUnifyingEntityMarker(((RequestFacade) request).getRequestURI());
        Map<String, Class<?>> currentEntityFieldsMap;
        if (!entitiesFieldsMap.containsKey(entityMarker)) {
            throw new BadRequestException("Entity:" + entityMarker + " is not exist or is not searchable!");
        }
        currentEntityFieldsMap = entitiesFieldsMap.get(entityMarker);
        List<RequestParameter> requestParameters = new ArrayList<>();
        request.getParameterMap().forEach((paramName, paramValue) -> {
            if (!currentEntityFieldsMap.containsKey(paramName)) {
                throw new BadRequestException("Field:" + paramName + " is not exist in searchable entity!");
            }
            Object typedParamValue = null;
            try {
                typedParamValue = requestValueTypeCaster(paramValue, currentEntityFieldsMap.get(paramName));
            } catch (ParseException e) {
                throw new BadRequestException("Wrong value type for:" + paramName + ", expected:" + currentEntityFieldsMap.get(paramName));
            }
            requestParameters.add(new RequestParameter(paramName, typedParamValue));
        });
        request.setAttribute("requestParameters", requestParameters);
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }

    private String getUnifyingEntityMarker(String requestURI) {
        String controllerRequestMappingValueWithoutSlash = requestURI.split("/")[1];
        return controllerRequestMappingValueWithoutSlash
                .substring(0, controllerRequestMappingValueWithoutSlash.length() - 1)
                .replace("-", "");
    }

    private Object requestValueTypeCaster(String[] requestValue, Class<?> castType) throws ParseException {
        String defaultTypeRequestValue = requestValue[0];
        Object castedRequestValue = defaultTypeRequestValue;
        if (isNumber(castType)) {
            NumberFormat numberFormat = NumberFormat.getInstance();
            castedRequestValue = castType.cast(numberFormat.parse(defaultTypeRequestValue));
        } else if (isLocalDate(castType)) {
            castedRequestValue = LocalDate.parse(defaultTypeRequestValue);
        } else if (isLocalTime(castType)) {
            castedRequestValue = LocalTime.parse(defaultTypeRequestValue);

        }
        return castedRequestValue;
    }

    private boolean isLocalTime(Class<?> castType) {
        return LocalTime.class.isAssignableFrom(castType);
    }

    private boolean isLocalDate(Class<?> castType) {
        return LocalDate.class.isAssignableFrom(castType);
    }

    private boolean isNumber(Class<?> castType) {
        return Number.class.isAssignableFrom(castType);
    }

    @Autowired
    public void setParametrizedSearchAnnotationAnalyser(ParametrizedSearchAnnotationAnalyser parametrizedSearchAnnotationAnalyser) {
        this.parametrizedSearchAnnotationAnalyser = parametrizedSearchAnnotationAnalyser;
    }
}
