package by.gsu.math.db_service.web.controller;

import by.gsu.math.db_service.dao.parameterizedsearch.ParametrizedSearchMethod;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Classroom;
import by.gsu.math.db_service.model.facade.classroom.ClassroomFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/classrooms")
public class ClassroomController {

    private ClassroomFacade classroomFacade;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    List<Classroom> findAll(final Pageable pageable) {
        return classroomFacade.findAll(pageable);
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    Classroom findById(@PathVariable @NotNull final Long id) {
        return classroomFacade.findById(id);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void save(@RequestBody final List<Classroom> classrooms) {
        classrooms.forEach(classroomFacade::save);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void update(@RequestBody final List<Classroom> classrooms) {
        classrooms.forEach(classroomFacade::save);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void deleteByIds(@RequestBody final List<Long> ids) {
        ids.forEach(classroomFacade::deleteById);
    }

    @ParametrizedSearchMethod(entityClass = Classroom.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/param-find", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Classroom> findByParameters(@RequestAttribute(value = "requestParameters") List<RequestParameter> params) {
        return classroomFacade.findByParam(params);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Classroom> findUpdatedRow(@RequestParam(value = "date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final LocalDateTime date) {
        return classroomFacade.findUpdatedRow(date);
    }

    @Autowired
    public void setClassroomFacade(final ClassroomFacade classroomFacade) {
        this.classroomFacade = classroomFacade;
    }
}
