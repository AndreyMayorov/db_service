package by.gsu.math.db_service.web.controller;

import by.gsu.math.db_service.dao.parameterizedsearch.ParametrizedSearchMethod;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.AcademicDegree;
import by.gsu.math.db_service.model.facade.academicdegree.AcademicDegreeFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/academic-degrees")
public class AcademicDegreeController {

    private AcademicDegreeFacade academicDegreeFacade;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    List<AcademicDegree> findAll(final Pageable pageable) {
        return academicDegreeFacade.findAll(pageable);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void save(@RequestBody final List<AcademicDegree> academicDegrees) {
        academicDegrees.forEach(academicDegreeFacade::save);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void update(@RequestBody final List<AcademicDegree> academicDegrees) {
        academicDegrees.forEach(academicDegreeFacade::save);
    }

    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deleteByIds(@RequestBody final List<Long> ids) {
        ids.forEach(academicDegreeFacade::deleteById);
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    AcademicDegree findById(@PathVariable @NotNull final Long id) {
        return academicDegreeFacade.findById(id);
    }

    @GetMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    List<AcademicDegree> findUpdatedRow(@RequestParam(value = "date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final LocalDateTime date) {
        return academicDegreeFacade.findUpdatedRow(date);
    }

    @ParametrizedSearchMethod(entityClass = AcademicDegree.class)
    @GetMapping(value = "/param-find", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    List<AcademicDegree> findByParameters(@RequestAttribute(value = "requestParameters") List<RequestParameter> params) {
        return academicDegreeFacade.findByParam(params);
    }

    @Autowired
    public void setAcademicDegreeFacade(final AcademicDegreeFacade academicDegreeFacade) {
        this.academicDegreeFacade = academicDegreeFacade;
    }


}
