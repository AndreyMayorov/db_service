package by.gsu.math.db_service.web.controller;

import by.gsu.math.db_service.dao.parameterizedsearch.ParametrizedSearchMethod;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Faculty;
import by.gsu.math.db_service.model.facade.faculty.FacultyFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/faculties")
public class FacultyController {

    private FacultyFacade facultyFacade;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    List<Faculty> findAll(final Pageable pageable) {
        return facultyFacade.findAll(pageable);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    Faculty findById(@PathVariable @NotNull final Long id) {
        return facultyFacade.findById(id);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void save(@RequestBody final List<Faculty> faculties) {
        faculties.forEach(facultyFacade::save);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void update(@RequestBody final List<Faculty> faculties) {
        faculties.forEach(facultyFacade::save);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    void deleteByIds(@RequestBody final List<Long> ids) {
        ids.forEach(facultyFacade::deleteById);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @ParametrizedSearchMethod(entityClass = Faculty.class)
    @GetMapping(value = "/param-find", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Faculty> findByParameters(@RequestAttribute(value = "requestParameters") List<RequestParameter> params) {
        return facultyFacade.findByParam(params);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Faculty> findUpdatedRow(@RequestParam(value = "date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final LocalDateTime date) {
        return facultyFacade.findUpdatedRow(date);
    }

    @Autowired
    public void setFacultyService(final FacultyFacade facultyFacade) {
        this.facultyFacade = facultyFacade;
    }
}
