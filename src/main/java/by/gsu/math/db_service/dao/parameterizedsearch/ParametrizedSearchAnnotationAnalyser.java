package by.gsu.math.db_service.dao.parameterizedsearch;

import org.apache.tomcat.util.collections.CaseInsensitiveKeyMap;
import org.reflections.Reflections;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class ParametrizedSearchAnnotationAnalyser {

    private Set<Class<?>> restControllerClasses;

    @PostConstruct
    public void setRestControllerClasses() {
        Reflections reflections = new Reflections("by.gsu.math.db_service.web.controller");
        restControllerClasses = reflections.getTypesAnnotatedWith(RestController.class);
    }

    public List<String> getParamFindControllerURLs() {
        return restControllerClasses.stream()
                .flatMap(controllerClass -> Arrays.stream(controllerClass.getDeclaredMethods()))
                .filter(this::isAnnotatedWithParametrizedSearchAnnotation)
                .map(this::getParametrizedSearchMethodURL).collect(Collectors.toList());
    }

    public Map<String, Map<String, Class<?>>> getEntitiesFieldsMap() {
        Map<String, Map<String, Class<?>>> entitiesFieldsMap = new CaseInsensitiveKeyMap<>();
        Set<Class<?>> entityClasses = new HashSet<>();

        restControllerClasses.stream()
                .flatMap(controllerClass -> Arrays.stream(controllerClass.getDeclaredMethods()))
                .filter(this::isAnnotatedWithParametrizedSearchAnnotation)
                .forEach(method -> entityClasses.add(getEntityClassFromParametrizedSearchAnnotation(method)));

        entityClasses.forEach(entityClass -> {
            String entityName = entityClass.getSimpleName();
            entitiesFieldsMap.put(entityName, getEntityFieldsForSearch(entityClass, new CaseInsensitiveKeyMap<>()));
        });
        return entitiesFieldsMap;
    }

    private Map<String, Class<?>> getEntityFieldsForSearch(Class<?> entityClass, Map<String, Class<?>> searchableEntityFields) {

        Arrays.stream(entityClass.getDeclaredFields())
                .filter(this::isSearchableField)
                .forEach(field -> searchableEntityFields.put(getSearchableFieldName(field), getSearchableFieldClass(field)));

        Class entitySuperClass = entityClass.getSuperclass();
        if (!isAnnotatedWithEntityAnnotation(entitySuperClass)) {
            return searchableEntityFields;
        }
        return getEntityFieldsForSearch(entitySuperClass, searchableEntityFields);
    }

    private String getSearchableFieldName(Field searchableField) {
        String searchableFieldName = searchableField.getName();
        if (isAnnotatedWithEntityAnnotation(searchableField.getType())) {
            searchableFieldName += "Id";
        }
        return searchableFieldName;
    }

    private Class<?> getSearchableFieldClass(Field searchableField) {
        Class<?> searchableFieldClass = searchableField.getType();
        if (isAnnotatedWithEntityAnnotation(searchableField.getType())) {
            searchableFieldClass = Long.class;
        }
        return searchableFieldClass;
    }

    private Class getEntityClassFromParametrizedSearchAnnotation(Method method) {
        return method.getAnnotation(ParametrizedSearchMethod.class).entityClass();
    }

    private String getParametrizedSearchMethodURL(Method annotatedMethod) {
        GetMapping getMappingMethodAnnotation = annotatedMethod.getAnnotation(GetMapping.class);
        RequestMapping requestMappingControllerAnnotation = annotatedMethod.getDeclaringClass().getAnnotation(RequestMapping.class);
        String methodURI = getMethodURI(getMappingMethodAnnotation);
        String controllerURI = getControllerURI(requestMappingControllerAnnotation);
        return controllerURI + methodURI;
    }

    private boolean isAnnotatedWithEntityAnnotation(Class<?> entityClass) {
        return entityClass.isAnnotationPresent(Entity.class);
    }

    private boolean isSearchableField(Field entityField) {
        return !Collection.class.isAssignableFrom(entityField.getType()) && !entityField.getName().equals("id") && !entityField.isAnnotationPresent(Embedded.class);
    }

    private boolean isAnnotatedWithParametrizedSearchAnnotation(Method method) {
        return method.isAnnotationPresent(ParametrizedSearchMethod.class);
    }

    private String getMethodURI(GetMapping getMappingMethodAnnotation) {
        String methodURI = "";
        if (getMappingMethodAnnotation.path().length > 0) {
            methodURI = getMappingMethodAnnotation.path()[0];
        } else if (getMappingMethodAnnotation.value().length > 0) {
            methodURI = getMappingMethodAnnotation.value()[0];
        }
        return methodURI;
    }

    private String getControllerURI(RequestMapping requestMappingControllerAnnotation) {
        String controllerURI = "";
        if (requestMappingControllerAnnotation.path().length > 0) {
            controllerURI = requestMappingControllerAnnotation.path()[0];
        } else if (requestMappingControllerAnnotation.value().length > 0) {
            controllerURI = requestMappingControllerAnnotation.value()[0];
        }
        return controllerURI;
    }
}

