package by.gsu.math.db_service.dao.repository;

import by.gsu.math.db_service.model.entity.Discipline;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface DisciplineRepository extends JpaRepository<Discipline, Long> {

    @Query(value = "select discipline from Discipline discipline where discipline.audit.updatedOn > :lastUpdateDate")
    List<Discipline> findUpdatedRow(LocalDateTime lastUpdateDate);
}
