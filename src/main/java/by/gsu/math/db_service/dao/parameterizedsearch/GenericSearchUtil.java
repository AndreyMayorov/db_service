package by.gsu.math.db_service.dao.parameterizedsearch;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class GenericSearchUtil<T> {

    @PersistenceContext
    private EntityManager entityManager;

    public List<T> findByParam(final List<RequestParameter> params, final Class<T> clazz) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);
        Root<T> root = criteriaQuery.from(clazz);
        criteriaQuery.select(root).where(getPredicates(params, criteriaBuilder, root));
        TypedQuery<T> query = entityManager.createQuery(criteriaQuery);
        return query.getResultList();
    }

    private Predicate[] getPredicates(final List<RequestParameter> params, final CriteriaBuilder criteriaBuilder, final Root<T> root) {
        List<Predicate> predicates = new ArrayList<>();

        if (params.stream().allMatch(Objects::isNull))
            return predicates.toArray(new Predicate[0]);

        params.forEach(requestParameter -> {
            if (!isRequestParameterValueNullable(requestParameter)) {
                if (isRequestParameterIdForInnerEntity(requestParameter)) {
                    Path<Long> childEntityIdPath = root.get(getEntityNameFromRequestParameterName(requestParameter)).get("id");
                    predicates.add(criteriaBuilder.equal(childEntityIdPath, requestParameter.getValue()));
                } else {
                    predicates.add(criteriaBuilder.equal(root.get(requestParameter.getName()), requestParameter.getValue()));
                }
            }
        });
        return predicates.toArray(new Predicate[0]);
    }

    private String getEntityNameFromRequestParameterName(RequestParameter requestParameter) {
        return StringUtils.removeEnd(requestParameter.getName(), "Id");
    }

    private boolean isRequestParameterValueNullable(RequestParameter requestParameter) {
        return Objects.isNull(requestParameter.getValue());
    }

    private boolean isRequestParameterIdForInnerEntity(RequestParameter requestParameter) {
        return requestParameter.getName().endsWith("Id");
    }
}
