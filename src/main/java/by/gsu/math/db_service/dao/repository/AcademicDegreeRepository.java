package by.gsu.math.db_service.dao.repository;

import by.gsu.math.db_service.model.entity.AcademicDegree;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface AcademicDegreeRepository extends JpaRepository<AcademicDegree, Long> {

    @Query(value = "select academicDegree from AcademicDegree academicDegree where academicDegree.audit.updatedOn > :lastUpdateDate")
    List<AcademicDegree> findUpdatedRow(LocalDateTime lastUpdateDate);
}
