package by.gsu.math.db_service.dao.repository;

import by.gsu.math.db_service.model.entity.Lesson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Repository
public interface LessonRepository extends JpaRepository<Lesson, Long>, JpaSpecificationExecutor<Lesson> {

    boolean existsById(Long id);

    @Transactional
    @Modifying
    @Query(value = "insert into Study_Hours(dtype, id, study_group_id, "
            + "discipline_id, teacher_id, classroom_id, study_hour_date, study_hour_time, faculty_id) "
            + "values(:dtype, :id, :study_group_id, :discipline_id, "
            + ":teacher_id, :classroom_id, :study_hour_date, :study_hour_time, :faculty_id)",
            nativeQuery = true)
    void insert(@Param("dtype") String type,
                @Param("id") Long id,
                @Param("study_group_id") Long groupId,
                @Param("discipline_id") Long disciplineId,
                @Param("teacher_id") Long teacherId,
                @Param("classroom_id") Long classroomId,
                @Param("study_hour_date") LocalDate studyHourDate,
                @Param("study_hour_time") LocalTime studyHourTime,
                @Param("faculty_id") Long facultyId);

    @Query(value = Constants.SELECT +
            "where study_hour_date between :beginDate and :endDate "
            + "and study_group_id = :study_group_id and dtype = 'Lesson' "
            + Constants.ORDER_BY,
            nativeQuery = true)
    List<Lesson> findByStudyHourDateBetweenAndStudyGroupId(
            @Param("beginDate") LocalDate beginDate,
            @Param("endDate") LocalDate endDate,
            @Param("study_group_id") Long studyGroupId);

    @Query(value = Constants.SELECT +
            "where study_hour_date between :beginDate and :endDate "
            + "and teacher_id = :teacher_id and dtype = 'Lesson' "
            + Constants.ORDER_BY,
            nativeQuery = true)
    List<Lesson> findByStudyHourDateBetweenAndTeacherId(
            @Param("beginDate") LocalDate beginDate,
            @Param("endDate") LocalDate endDate,
            @Param("teacher_id") Long teacherId);

    @Query(value = Constants.SELECT +
            "where study_hour_date between :beginDate and :endDate "
            + "and classroom_id = :classroom_id and dtype = 'Lesson' "
            + Constants.ORDER_BY,
            nativeQuery = true)
    List<Lesson> findByStudyHourDateBetweenAndClassroomId(
            @Param("beginDate") LocalDate beginDate,
            @Param("endDate") LocalDate endDate,
            @Param("classroom_id") Long classroomId);

    List<Lesson> findByStudyHourDateBetween(LocalDate startDate, LocalDate endDate);

    List<Lesson> findByStudyHourDateBetweenAndStudyHourTimeBetween(LocalDate startDate, LocalDate endDate, LocalTime startTime, LocalTime endTime);

}

final class Constants {

    static final String SELECT = "Select * From study_Hours ";
    static final String ORDER_BY = "Order By study_hour_date, study_hour_time ";

    private Constants() {
    }
}
