package by.gsu.math.db_service.dao.repository;

import by.gsu.math.db_service.model.entity.Chair;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ChairRepository extends JpaRepository<Chair, Long> {

    @Query(value = "select chair from Chair chair where chair.audit.updatedOn > :lastUpdateDate")
    List<Chair> findUpdatedRow(LocalDateTime lastUpdateDate);

}
