package by.gsu.math.db_service.dao.parameterizedsearch;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RequestParameter {

    String name;
    Object value;
}
