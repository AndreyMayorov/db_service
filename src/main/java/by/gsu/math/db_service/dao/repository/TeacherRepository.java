package by.gsu.math.db_service.dao.repository;

import by.gsu.math.db_service.model.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Long> {

    @Query(value = "select teacher from Teacher teacher where teacher.audit.updatedOn > :lastUpdateDate")
    List<Teacher> findUpdatedRow(LocalDateTime lastUpdateDate);
}
