package by.gsu.math.db_service.dao.repository;

import by.gsu.math.db_service.model.entity.TeacherRank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface TeacherRankRepository extends JpaRepository<TeacherRank, Long> {

    @Query(value = "select teacherRank from TeacherRank teacherRank where teacherRank.audit.updatedOn > :lastUpdateDate")
    List<TeacherRank> findUpdatedRow(LocalDateTime lastUpdateDate);
}
