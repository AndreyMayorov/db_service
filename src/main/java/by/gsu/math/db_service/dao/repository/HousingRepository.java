package by.gsu.math.db_service.dao.repository;

import by.gsu.math.db_service.model.entity.Housing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface HousingRepository extends JpaRepository<Housing, Long> {

    @Query(value = "select housing from Housing housing where housing.audit.updatedOn > :lastUpdateDate")
    List<Housing> findUpdatedRow(LocalDateTime lastUpdateDate);
}
