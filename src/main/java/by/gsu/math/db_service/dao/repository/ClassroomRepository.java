package by.gsu.math.db_service.dao.repository;

import by.gsu.math.db_service.model.entity.Classroom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface ClassroomRepository extends JpaRepository<Classroom, Long> {

    @Query(value = "SELECT * FROM classrooms WHERE " +
            "classroom_name = :classroom_name and housing_id = (SELECT id FROM housings WHERE housing_name = :housing_name)", nativeQuery = true)
    Optional<Classroom> findByClassroomNameAndHousingName(@Param("classroom_name") String classroomName,
                                                          @Param("housing_name") String housingName);

    @Query(value = "select classroom from Classroom classroom where classroom.audit.updatedOn > :lastUpdateDate")
    List<Classroom> findUpdatedRow(LocalDateTime lastUpdateDate);
}
