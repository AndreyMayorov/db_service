package by.gsu.math.db_service.dao.repository;

import by.gsu.math.db_service.model.entity.StudyGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface StudyGroupRepository extends JpaRepository<StudyGroup, Long> {

    @Query(value = "select studyGroup from StudyGroup studyGroup where studyGroup.audit.updatedOn > :lastUpdateDate")
    List<StudyGroup> findUpdatedRow(LocalDateTime lastUpdateDate);
}
