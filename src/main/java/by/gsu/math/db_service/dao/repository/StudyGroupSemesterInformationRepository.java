package by.gsu.math.db_service.dao.repository;

import by.gsu.math.db_service.model.entity.StudyGroupSemesterInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudyGroupSemesterInformationRepository extends JpaRepository<StudyGroupSemesterInformation, Long> {
}
