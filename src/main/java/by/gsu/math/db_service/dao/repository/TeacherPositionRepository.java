package by.gsu.math.db_service.dao.repository;

import by.gsu.math.db_service.model.entity.TeacherPosition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface TeacherPositionRepository extends JpaRepository<TeacherPosition, Long> {

    @Query(value = "select teacherPosition from TeacherPosition teacherPosition where teacherPosition.audit.updatedOn > :lastUpdateDate")
    List<TeacherPosition> findUpdatedRow(LocalDateTime lastUpdateDate);
}
