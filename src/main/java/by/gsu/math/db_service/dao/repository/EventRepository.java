package by.gsu.math.db_service.dao.repository;

import by.gsu.math.db_service.model.entity.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Long>, JpaSpecificationExecutor<Event> {

    boolean existsById(Long id);

    @Transactional
    @Modifying
    @Query(value = "insert into Study_Hours(dtype, id, study_group_id, study_hour_date, "
            + "study_hour_time, description, speaker, place,faculty_id) "
            + "values(:dtype, :id, :study_group_id, :study_hour_date, "
            + ":study_hour_time, :description, :speaker, :place,:faculty_id)", nativeQuery = true)
    void insert(@Param("dtype") String type,
                @Param("id") Long id,
                @Param("study_group_id") Long groupId,
                @Param("study_hour_date") LocalDate studyHourDate,
                @Param("study_hour_time") LocalTime studyHourTime,
                @Param("description") String description,
                @Param("speaker") String speaker,
                @Param("place") String place,
                @Param("faculty_id") Long facultyId);

    @Query(value = "select * from Study_Hours where study_hour_date between :beginDate and :endDate "
            + "and study_group_id = :study_group_id and dtype = 'Event' "
            + "Order By study_hour_date, study_hour_time ",
            nativeQuery = true)
    List<Event> findByStudyHourDateBetweenAndStudyGroupId(
            @Param("beginDate") LocalDate beginDate,
            @Param("endDate") LocalDate endDate,
            @Param("study_group_id") Long studyGroupId);


    @Query(value = "select * from Study_Hours where study_hour_date between :beginDate and :endDate "
            + "and teacher_id = :teacher_id and dtype = 'Event' "
            + "Order By study_hour_date, study_hour_time ",
            nativeQuery = true)
    List<Event> findByStudyHourDateBetweenAndTeacherId(
            @Param("beginDate") LocalDate beginDate,
            @Param("endDate") LocalDate endDate,
            @Param("teacher_id") Long teacherId);


    @Query(value = "select * from Study_Hours where study_hour_date between :beginDate and :endDate "
            + "and classroom_id = :classroom_id and dtype = 'Event' "
            + "Order By study_hour_date, study_hour_time ",
            nativeQuery = true)
    List<Event> findByStudyHourDateBetweenAndClassroomId(
            @Param("beginDate") LocalDate beginDate,
            @Param("endDate") LocalDate endDate,
            @Param("classroom_id") Long classroomId);

    List<Event> findByStudyHourDateBetween(LocalDate startDate, LocalDate endDate);

    List<Event> findByStudyHourDateBetweenAndStudyHourTimeBetween(LocalDate startDate, LocalDate endDate, LocalTime startTime, LocalTime endTime);

}
