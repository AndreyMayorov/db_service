package by.gsu.math.db_service.dao.repository;

import by.gsu.math.db_service.model.entity.Faculty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface FacultyRepository extends JpaRepository<Faculty, Long>, JpaSpecificationExecutor<Faculty> {

    @Query(value = "select faculty from Faculty faculty where faculty.audit.updatedOn > :lastUpdateDate")
    List<Faculty> findUpdatedRow(LocalDateTime lastUpdateDate);
}
