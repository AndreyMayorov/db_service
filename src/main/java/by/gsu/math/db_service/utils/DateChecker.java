package by.gsu.math.db_service.utils;

import by.gsu.math.db_service.exception.BadRequestException;
import lombok.experimental.UtilityClass;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@UtilityClass
public class DateChecker {

    private static final Long COUNT_OF_DAYS_IN_TWO_WEEKS = 14L;
    private static final String TOO_LARGE_DATE_PERIOD_MESSAGE = "The difference between dates must be less than or equal to 14";


    public void validateDatePeriod(LocalDate startDate, LocalDate endDate) {
        if (ChronoUnit.DAYS.between(startDate, endDate) > COUNT_OF_DAYS_IN_TWO_WEEKS) {
            throw new BadRequestException(TOO_LARGE_DATE_PERIOD_MESSAGE);
        }
    }
}
