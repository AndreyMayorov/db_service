package by.gsu.math.db_service.model.facade.chair;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Chair;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

public interface ChairFacade {

    Chair findById(Long id);

    List<Chair> findAll(Pageable pageable);

    void save(Chair chair);

    void deleteById(Long id);

    List<Chair> findByParam(List<RequestParameter> params);

    List<Chair> findUpdatedRow(LocalDateTime lastUpdateDate);
}
