package by.gsu.math.db_service.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "teacher_positions")
@NoArgsConstructor
@AllArgsConstructor
public class TeacherPosition {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 55)
    @Column(name = "teacher_position_name", unique = true)
    private String name;

    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "teacherPosition")
    @JsonIgnore
    private Set<Teacher> teachers = new HashSet<>();

    @Embedded
    @JsonIgnore
    private Audit audit = new Audit();
}
