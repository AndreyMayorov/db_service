package by.gsu.math.db_service.model.facade.teacherposition;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.TeacherPosition;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

public interface TeacherPositionFacade {

    TeacherPosition findById(Long id);

    List<TeacherPosition> findAll(Pageable pageable);

    void save(final TeacherPosition teacherPosition);

    void deleteById(Long id);

    List<TeacherPosition> findByParam(List<RequestParameter> params);

    List<TeacherPosition> findUpdatedRow(LocalDateTime lastUpdateDate);
}
