package by.gsu.math.db_service.model.facade.teacherrank;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.TeacherRank;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

public interface TeacherRankFacade {

    TeacherRank findById(Long id);

    List<TeacherRank> findAll(Pageable pageable);

    void save(TeacherRank teacherRank);

    void deleteById(Long id);

    List<TeacherRank> findByParam(List<RequestParameter> params);

    List<TeacherRank> findUpdatedRow(LocalDateTime lastUpdateDate);
}
