package by.gsu.math.db_service.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Entity
@Table(name = "Classrooms",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"housing_id", "classroom_name"})
        })
@NoArgsConstructor
@AllArgsConstructor
public class Classroom {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @NotNull
    private Housing housing;

    @NotNull
    @Size(min = 1, max = 55)
    @Column(name = "classroom_name")
    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "classroom", targetEntity = Lesson.class)
    @JsonIgnore
    private List<StudyHour> lessons;

    @ManyToOne
    @NotNull
    private Faculty faculty;

    @NotNull
    @Column(name = "count_of_seats")
    private Long countOfSeats;

    @Embedded
    @JsonIgnore
    private Audit audit = new Audit();
}
