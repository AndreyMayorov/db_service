package by.gsu.math.db_service.model.facade.classroom.implementation;

import by.gsu.math.db_service.Constant;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.exception.NotFoundException;
import by.gsu.math.db_service.model.entity.Classroom;
import by.gsu.math.db_service.model.facade.classroom.ClassroomFacade;
import by.gsu.math.db_service.service.classroom.ClassroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class ClassroomFacadeImplementation implements ClassroomFacade {

    private ClassroomService classroomService;

    @Override
    public Classroom findById(final Long id) {
        return classroomService.findById(id).orElseThrow(()
                -> new NotFoundException(Constant.NOT_FOUND_RESOURCE_MESSAGE));
    }

    @Override
    public List<Classroom> findAll(final Pageable pageable) {
        return classroomService.findAll(pageable);
    }

    @Override
    public void save(final Classroom classroom) {
        classroomService.save(classroom);
    }

    @Override
    public void deleteById(final Long id) {
        classroomService.deleteById(id);
    }

    @Override
    public List<Classroom> findByParam(List<RequestParameter> params) {
        return classroomService.findByParam(params);
    }

    @Override
    public List<Classroom> findUpdatedRow(final LocalDateTime lastUpdateDate) {
        return classroomService.findUpdatedRow(lastUpdateDate);
    }

    @Autowired
    public void setClassroomService(final ClassroomService classroomService) {
        this.classroomService = classroomService;
    }
}
