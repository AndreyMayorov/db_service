package by.gsu.math.db_service.model.facade.event.implementation;

import by.gsu.math.db_service.Constant;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.exception.NotFoundException;
import by.gsu.math.db_service.model.entity.Event;
import by.gsu.math.db_service.model.facade.event.EventFacade;
import by.gsu.math.db_service.service.event.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Component
public class EventFacadeImplementation implements EventFacade {

    private EventService eventService;

    @Override
    public Event findById(final Long id) {
        return eventService.findById(id).orElseThrow(()
                -> new NotFoundException(Constant.NOT_FOUND_RESOURCE_MESSAGE));
    }

    @Override
    public List<Event> findAll(final Pageable pageable, final Specification<Event> lessonSpecification) {
        return eventService.findAll(pageable, lessonSpecification);
    }

    @Override
    public List<Event> findByStudyHourDateBetweenAndStudyGroupId(final LocalDate beginDate, final LocalDate endDate, final Long groupId) {
        return eventService.findByStudyHourDateBetweenAndStudyGroup(beginDate, endDate, groupId);
    }

    @Override
    public List<Event> findByStudyHourDateBetweenAndTeacherId(final LocalDate beginDate, final LocalDate endDate, final Long teacherId) {
        return eventService.findByStudyHourDateBetweenAndTeacherId(beginDate, endDate, teacherId);
    }

    @Override
    public List<Event> findByStudyHourDateBetweenAndClassroomId(final LocalDate beginDate, final LocalDate endDate, final Long classroomId) {
        return eventService.findByStudyHourDateBetweenAndClassroomId(beginDate, endDate, classroomId);
    }

    @Override
    public List<Event> findByParam(List<RequestParameter> params) {
        return eventService.findByParam(params);
    }

    @Override
    public List<Event> findByStudyHourDateBetween(final LocalDate startDate, final LocalDate endDate) {
        return eventService.findByStudyHourDateBetween(startDate, endDate);
    }

    @Override
    public List<Event> findByStudyHourDateBetweenAndStudyHourTimeBetween(final LocalDate startDate, final LocalDate endDate,
                                                                         final LocalTime startTime, final LocalTime endTime) {
        return eventService.findByStudyHourDateBetweenAndStudyHourTimeBetween(startDate, endDate, startTime, endTime);
    }

    @Override
    public void save(final Event event) {
        eventService.save(event);
    }

    @Override
    public void deleteById(final Long id) {
        eventService.deleteById(id);
    }

    @Override
    public List<Event> findAll(final Pageable pageable) {
        return eventService.findAll(pageable);
    }

    @Autowired
    public void setEventService(final EventService eventService) {
        this.eventService = eventService;
    }
}
