package by.gsu.math.db_service.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Entity
@NoArgsConstructor
@DiscriminatorValue(value = "Event")
public class Event extends StudyHour {

    @Column(length = 500)
    @NotNull
    @Size(min = 5, max = 500)
    private String description;

    @Size(min = 3, max = 55)
    private String speaker;

    @NotNull
    @Size(min = 3, max = 55)
    private String place;

    public Event(Long id,
                 @NotNull StudyGroup studyGroup,
                 @NotNull LocalDate studyHourDate,
                 @NotNull LocalTime studyHourTime,
                 @NotNull String description,
                 String speaker,
                 @NotNull String place,
                 @NotNull Faculty faculty) {
        super(id, studyGroup, studyHourDate, studyHourTime, faculty);
        this.description = description;
        this.speaker = speaker;
        this.place = place;
    }
}