package by.gsu.math.db_service.model.facade.group;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.StudyGroup;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

public interface StudyGroupFacade {

    StudyGroup findById(Long id);

    List<StudyGroup> findAll(Pageable pageable);

    void save(StudyGroup studyGroup);

    void deleteById(Long id);

    List<StudyGroup> findByParam(List<RequestParameter> params);

    List<StudyGroup> findUpdatedRow(LocalDateTime lastUpdateDate);
}
