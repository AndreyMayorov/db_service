package by.gsu.math.db_service.model.facade.academicdegree;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.AcademicDegree;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

public interface AcademicDegreeFacade {

    AcademicDegree findById(Long id);

    List<AcademicDegree> findAll(Pageable pageable);

    void deleteById(Long id);

    void save(AcademicDegree academicDegree);

    List<AcademicDegree> findByParam(List<RequestParameter> params);

    List<AcademicDegree> findUpdatedRow(LocalDateTime lastUpdatedDate);
}
