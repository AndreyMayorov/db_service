package by.gsu.math.db_service.model.facade.classroom;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Classroom;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

public interface ClassroomFacade {

    Classroom findById(Long id);

    List<Classroom> findAll(Pageable pageable);

    void save(Classroom classroom);

    void deleteById(Long id);

    List<Classroom> findByParam(List<RequestParameter> params);

    List<Classroom> findUpdatedRow(LocalDateTime lastUpdateDate);
}
