package by.gsu.math.db_service.model.facade.lesson.implementation;

import by.gsu.math.db_service.Constant;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.exception.NotFoundException;
import by.gsu.math.db_service.model.entity.Lesson;
import by.gsu.math.db_service.model.facade.lesson.LessonFacade;
import by.gsu.math.db_service.service.lesson.LessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Component
public class LessonFacadeImplementation implements LessonFacade {

    private LessonService lessonService;

    @Override
    public Lesson findById(final Long id) {
        return lessonService.findById(id).orElseThrow(()
                -> new NotFoundException(Constant.NOT_FOUND_RESOURCE_MESSAGE));
    }

    @Override
    public List<Lesson> findAll(final Pageable pageable, final Specification<Lesson> lessonSpecification) {
        return lessonService.findAll(pageable, lessonSpecification);
    }

    @Override
    public List<Lesson> findAll(final Pageable pageable) {
        return lessonService.findAll(pageable);
    }

    @Override
    public void templateSave(final List<List<Lesson>> schedule) {
        lessonService.templateSave(schedule);
    }

    @Override
    public void save(final Lesson lesson) {
        lessonService.save(lesson);
    }

    @Override
    public List<Lesson> findByStudyHourDateBetweenAndStudyGroupId(final LocalDate beginDate, final LocalDate endDate, final Long groupId) {
        return lessonService.findByStudyHourDateBetweenAndStudyGroup(beginDate, endDate, groupId);
    }

    @Override
    public void swap(final Long firstLessonId, final Long secondLessonId) {
        Lesson firstLesson = lessonService.findById(firstLessonId).orElseThrow(()
                -> new NotFoundException(Constant.NOT_FOUND_RESOURCE_MESSAGE));

        Lesson secondLesson = lessonService.findById(secondLessonId).orElseThrow(()
                -> new NotFoundException(Constant.NOT_FOUND_RESOURCE_MESSAGE));

        LocalDate tempDate = firstLesson.getStudyHourDate();
        LocalTime tempTime = firstLesson.getStudyHourTime();
        firstLesson.setStudyHourDate(secondLesson.getStudyHourDate());
        firstLesson.setStudyHourTime(secondLesson.getStudyHourTime());
        secondLesson.setStudyHourTime(tempTime);
        secondLesson.setStudyHourDate(tempDate);
        lessonService.save(firstLesson);
        lessonService.save(secondLesson);
    }

    @Override
    public void deleteById(final Long id) {
        lessonService.deleteById(id);
    }

    @Override
    public void setDateAndTime(final LocalDate date, final LocalTime time, final Long lessonId) {
        Lesson lesson = lessonService.findById(lessonId).orElseThrow(()
                -> new NotFoundException(Constant.NOT_FOUND_RESOURCE_MESSAGE));
        lesson.setStudyHourDate(date);
        lesson.setStudyHourTime(time);
        lessonService.save(lesson);
    }

    @Override
    public List<Lesson> findByStudyHourDateBetweenAndTeacherId(final LocalDate beginDate, final LocalDate endDate, final Long teacherId) {
        return lessonService.findByStudyHourDateBetweenAndTeacherId(beginDate, endDate, teacherId);
    }

    @Override
    public List<Lesson> findByStudyHourDateBetweenAndClassroomId(final LocalDate beginDate, final LocalDate endDate, final Long classroomId) {
        return lessonService.findByStudyHourDateBetweenAndClassroomId(beginDate, endDate, classroomId);
    }

    @Override
    public List<Lesson> findByParam(List<RequestParameter> params) {
        return lessonService.findByParam(params);
    }

    @Override
    public List<Lesson> findByStudyHourDateBetween(final LocalDate startDate, final LocalDate endDate) {
        return lessonService.findByStudyHourDateBetween(startDate, endDate);
    }

    @Override
    public List<Lesson> findByStudyHourDateBetweenAndStudyHourTimeBetween(final LocalDate startDate, final LocalDate endDate,
                                                                          final LocalTime startTime, final LocalTime endTime) {
        return lessonService.findByStudyHourDateBetweenAndStudyHourTimeBetween(startDate, endDate, startTime, endTime);
    }

    @Autowired
    public void setLessonService(final LessonService lessonService) {
        this.lessonService = lessonService;
    }

}
