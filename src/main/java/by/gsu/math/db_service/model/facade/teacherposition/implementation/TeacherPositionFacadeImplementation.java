package by.gsu.math.db_service.model.facade.teacherposition.implementation;

import by.gsu.math.db_service.Constant;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.exception.NotFoundException;
import by.gsu.math.db_service.model.entity.TeacherPosition;
import by.gsu.math.db_service.model.facade.teacherposition.TeacherPositionFacade;
import by.gsu.math.db_service.service.teacherposition.TeacherPositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class TeacherPositionFacadeImplementation implements TeacherPositionFacade {

    private TeacherPositionService teacherPositionService;

    @Override
    public TeacherPosition findById(final Long id) {
        return teacherPositionService.findById(id).orElseThrow(()
                -> new NotFoundException(Constant.NOT_FOUND_RESOURCE_MESSAGE));
    }

    @Override
    public List<TeacherPosition> findAll(final Pageable pageable) {
        return teacherPositionService.findAll(pageable);
    }

    @Override
    public void save(final TeacherPosition teacherPosition) {
        teacherPositionService.save(teacherPosition);
    }

    @Override
    public void deleteById(final Long id) {
        teacherPositionService.deleteById(id);
    }

    @Override
    public List<TeacherPosition> findByParam(List<RequestParameter> params) {
        return teacherPositionService.findByParam(params);
    }

    @Override
    public List<TeacherPosition> findUpdatedRow(final LocalDateTime lastUpdateDate) {
        return teacherPositionService.findUpdatedRow(lastUpdateDate);
    }

    @Autowired
    public void setTeacherPositionService(final TeacherPositionService teacherPositionService) {
        this.teacherPositionService = teacherPositionService;
    }

}
