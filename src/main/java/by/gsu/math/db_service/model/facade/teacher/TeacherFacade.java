package by.gsu.math.db_service.model.facade.teacher;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Teacher;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

public interface TeacherFacade {

    Teacher findById(Long id);

    List<Teacher> findAll(Pageable pageable);

    void save(Teacher teacher);

    void deleteById(Long id);

    List<Teacher> findByParam(List<RequestParameter> params);

    List<Teacher> findUpdatedRow(LocalDateTime lastUpdateDate);
}
