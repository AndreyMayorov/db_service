package by.gsu.math.db_service.model.facade.housing.implementation;

import by.gsu.math.db_service.Constant;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.exception.NotFoundException;
import by.gsu.math.db_service.model.entity.Housing;
import by.gsu.math.db_service.model.facade.housing.HousingFacade;
import by.gsu.math.db_service.service.housing.HousingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class HousingFacadeImplementation implements HousingFacade {

    private HousingService housingService;

    @Override
    public Housing findById(final Long id) {
        return housingService.findById(id).orElseThrow(()
                -> new NotFoundException(Constant.NOT_FOUND_RESOURCE_MESSAGE));
    }

    @Override
    public List<Housing> findAll(final Pageable pageable) {
        return housingService.findAll(pageable);
    }

    @Override
    public void save(final Housing housing) {
        housingService.save(housing);
    }

    @Override
    public void deleteById(final Long id) {
        housingService.deleteById(id);
    }

    @Override
    public List<Housing> findByParam(List<RequestParameter> params) {
        return housingService.findByParam(params);
    }

    @Override
    public List<Housing> findUpdatedRow(final LocalDateTime lastUpdateDate) {
        return housingService.findUpdatedRow(lastUpdateDate);
    }

    @Autowired
    public void setHousingService(final HousingService housingService) {
        this.housingService = housingService;
    }

}
