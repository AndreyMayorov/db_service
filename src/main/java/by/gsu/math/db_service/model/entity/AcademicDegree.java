package by.gsu.math.db_service.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "academic_degree")
public class AcademicDegree {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 55)
    @Column(name = "academic_degree_name", unique = true)
    private String name;

    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "academicDegree")
    @JsonIgnore
    private List<Teacher> teachers = new ArrayList<>();

    @Embedded
    @JsonIgnore
    private Audit audit = new Audit();
}
