package by.gsu.math.db_service.model.facade.chair.implementation;

import by.gsu.math.db_service.Constant;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.exception.NotFoundException;
import by.gsu.math.db_service.model.entity.Chair;
import by.gsu.math.db_service.model.facade.chair.ChairFacade;
import by.gsu.math.db_service.service.chair.ChairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class ChairFacadeImplementation implements ChairFacade {

    private ChairService chairService;

    @Override
    public Chair findById(final Long id) {
        return chairService.findById(id).orElseThrow(()
                -> new NotFoundException(Constant.NOT_FOUND_RESOURCE_MESSAGE));
    }

    @Override
    public List<Chair> findAll(final Pageable pageable) {
        return chairService.findAll(pageable);
    }

    @Override
    public void save(final Chair chair) {
        chairService.save(chair);
    }

    @Override
    public void deleteById(final Long id) {
        chairService.deleteById(id);
    }

    @Override
    public List<Chair> findByParam(List<RequestParameter> params) {
        return chairService.findByParam(params);
    }

    @Override
    public List<Chair> findUpdatedRow(final LocalDateTime lastUpdateDate) {
        return chairService.findUpdatedRow(lastUpdateDate);
    }

    @Autowired
    public void setChairService(final ChairService chairService) {
        this.chairService = chairService;
    }
}
