package by.gsu.math.db_service.model.facade.faculty.implementation;

import by.gsu.math.db_service.Constant;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.exception.NotFoundException;
import by.gsu.math.db_service.model.entity.Faculty;
import by.gsu.math.db_service.model.facade.faculty.FacultyFacade;
import by.gsu.math.db_service.service.faculty.FacultyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class FacultyFacadeImplementation implements FacultyFacade {

    private FacultyService facultyService;

    @Override
    public Faculty findById(final Long id) {
        return facultyService.findById(id).orElseThrow(()
                -> new NotFoundException(Constant.NOT_FOUND_RESOURCE_MESSAGE));
    }

    @Override
    public List<Faculty> findAll(final Pageable pageable) {
        return facultyService.findAll(pageable);
    }

    @Override
    public void save(final Faculty faculty) {
        facultyService.save(faculty);
    }

    @Override
    public void deleteById(final Long id) {
        facultyService.deleteById(id);
    }

    @Override
    public List<Faculty> findByParam(List<RequestParameter> params) {
        return facultyService.findByParam(params);
    }

    @Override
    public List<Faculty> findUpdatedRow(final LocalDateTime lastUpdateDate) {
        return facultyService.findUpdatedRow(lastUpdateDate);
    }

    @Autowired
    public void setFacultyService(final FacultyService facultyService) {
        this.facultyService = facultyService;
    }
}
