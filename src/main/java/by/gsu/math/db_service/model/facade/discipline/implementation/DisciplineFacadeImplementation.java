package by.gsu.math.db_service.model.facade.discipline.implementation;

import by.gsu.math.db_service.Constant;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.exception.NotFoundException;
import by.gsu.math.db_service.model.entity.Discipline;
import by.gsu.math.db_service.model.facade.discipline.DisciplineFacade;
import by.gsu.math.db_service.service.discipline.DisciplineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class DisciplineFacadeImplementation implements DisciplineFacade {

    private DisciplineService disciplineService;

    @Override
    public Discipline findById(final Long id) {
        return disciplineService.findById(id).orElseThrow(()
                -> new NotFoundException(Constant.NOT_FOUND_RESOURCE_MESSAGE));
    }

    @Override
    public List<Discipline> findAll(final Pageable pageable) {
        return disciplineService.findAll(pageable);
    }

    @Override
    public void save(final Discipline discipline) {
        disciplineService.save(discipline);
    }

    @Override
    public void deleteById(final Long id) {
        disciplineService.deleteById(id);
    }

    @Override
    public List<Discipline> findByParam(List<RequestParameter> params) {
        return disciplineService.findByParam(params);
    }

    @Override
    public List<Discipline> findUpdatedRow(final LocalDateTime lastUpdateDate) {
        return disciplineService.findUpdatedRow(lastUpdateDate);
    }

    @Autowired
    public void setDisciplineService(final DisciplineService disciplineService) {
        this.disciplineService = disciplineService;
    }
}
