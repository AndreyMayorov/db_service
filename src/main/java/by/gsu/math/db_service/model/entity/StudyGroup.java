package by.gsu.math.db_service.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Entity
@Table(name = "study_groups")
@NoArgsConstructor
@AllArgsConstructor
public class StudyGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "full_name", unique = true)
    private String fullName;

    @NotNull
    @Size(min = 1, max = 55)
    private String shortName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "studyGroup")
    @JsonIgnore
    private List<StudyHour> lessons;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "studyGroup")
    @JsonIgnore
    private List<StudyGroupSemesterInformation> studyGroupSemestersInformation;

    @ManyToOne
    @NotNull
    private Faculty faculty;

    @Embedded
    @JsonIgnore
    private Audit audit = new Audit();
}
