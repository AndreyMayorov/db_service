package by.gsu.math.db_service.model.facade.faculty;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Faculty;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

public interface FacultyFacade {

    Faculty findById(Long id);

    List<Faculty> findAll(Pageable pageable);

    void save(Faculty faculty);

    void deleteById(Long id);

    List<Faculty> findByParam(List<RequestParameter> params);

    List<Faculty> findUpdatedRow(LocalDateTime lastUpdateDate);
}
