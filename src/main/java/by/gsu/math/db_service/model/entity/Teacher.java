package by.gsu.math.db_service.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

@Data
@Entity
@Table(name = "teachers")
@NoArgsConstructor
@AllArgsConstructor
public class Teacher {

    @Id
    private Long id;

    @Size(max = 55)
    @Column(name = "first_name")
    private String firstName;

    @Size(max = 55)
    @Column(name = "second_name")
    private String secondName;

    @Size(max = 55)
    @Column(name = "last_name")
    private String lastName;

    @Column(name = "employee_rate")
    private BigDecimal employeeRate;

    @Column(name = "employee_type")
    private String employeeType;

    @Column(name = "photo_url")
    private String photoUrl;

    @ManyToOne
    private Chair chair;

    @ManyToOne
    private TeacherRank teacherRank;

    @ManyToOne
    private AcademicDegree academicDegree;

    @ManyToOne
    private TeacherPosition teacherPosition;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "teacher", targetEntity = Lesson.class)
    @JsonIgnore
    private List<StudyHour> lessons;

    @Embedded
    @JsonIgnore
    private Audit audit = new Audit();
}
