package by.gsu.math.db_service.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Entity
@Table(name = "faculties")
@NoArgsConstructor
@AllArgsConstructor
public class Faculty {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(min = 5, max = 55)
    @Column(name = "faculty_name", unique = true)
    private String name;

    @Column(name = "faculty_description")
    private String description;

    @Column(name = "dean")
    private String dean;

    @Column(name = "displayed")
    private Boolean displayed;

    @Column(name = "address")
    private String address;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "faculty")
    private List<Lesson> lessons;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "faculty")
    @JsonIgnore
    private List<StudyGroup> studyGroups;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "faculty")
    @JsonIgnore
    private List<Chair> chairs;

    @Embedded
    @JsonIgnore
    private Audit audit = new Audit();
}
