package by.gsu.math.db_service.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@Table(name = "study_group_semester_information")
public class StudyGroupSemesterInformation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDate semesterStartDate;

    private LocalDate semesterEndDate;

    @ManyToOne(targetEntity = StudyGroup.class)
    private StudyGroup studyGroup;
}
