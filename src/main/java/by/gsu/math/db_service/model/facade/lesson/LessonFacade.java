package by.gsu.math.db_service.model.facade.lesson;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Lesson;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface LessonFacade {

    Lesson findById(Long id);

    List<Lesson> findAll(Pageable pageable, Specification<Lesson> lessonSpecification);

    List<Lesson> findAll(Pageable pageable);

    void save(Lesson lesson);

    List<Lesson> findByStudyHourDateBetweenAndStudyGroupId(LocalDate beginDate, LocalDate endDate, Long groupId);

    void deleteById(Long id);

    void swap(Long firstLessonId, Long secondLessonId);

    void templateSave(List<List<Lesson>> schedule);

    void setDateAndTime(LocalDate date, LocalTime time, Long lessonId);

    List<Lesson> findByStudyHourDateBetweenAndTeacherId(LocalDate beginDate, LocalDate endDate, Long teacherId);

    List<Lesson> findByStudyHourDateBetweenAndClassroomId(LocalDate beginDate, LocalDate endDate, Long classroomId);

    List<Lesson> findByParam(List<RequestParameter> params);

    List<Lesson> findByStudyHourDateBetween(LocalDate startDate, LocalDate endDate);

    List<Lesson> findByStudyHourDateBetweenAndStudyHourTimeBetween(LocalDate startDate, LocalDate endDate, LocalTime startTime, LocalTime endTime);
}
