package by.gsu.math.db_service.model.facade.event;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Event;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface EventFacade {

    Event findById(Long id);

    List<Event> findAll(Pageable pageable, Specification<Event> lessonSpecification);

    List<Event> findByStudyHourDateBetweenAndStudyGroupId(LocalDate beginDate, LocalDate endDate, Long groupId);

    void save(Event event);

    void deleteById(Long id);

    List<Event> findAll(Pageable pageable);

    List<Event> findByStudyHourDateBetweenAndTeacherId(LocalDate beginDate, LocalDate endDate, Long teacherId);

    List<Event> findByStudyHourDateBetweenAndClassroomId(LocalDate beginDate, LocalDate endDate, Long classroomId);

    List<Event> findByParam(List<RequestParameter> params);

    List<Event> findByStudyHourDateBetween(LocalDate startDate, LocalDate endDate);

    List<Event> findByStudyHourDateBetweenAndStudyHourTimeBetween(LocalDate startDate, LocalDate endDate, LocalTime startTime, LocalTime endTime);
}
