package by.gsu.math.db_service.model.facade.teacher.implementation;

import by.gsu.math.db_service.Constant;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.exception.NotFoundException;
import by.gsu.math.db_service.model.entity.Teacher;
import by.gsu.math.db_service.model.facade.teacher.TeacherFacade;
import by.gsu.math.db_service.service.teacher.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class TeacherFacadeImplementation implements TeacherFacade {

    private TeacherService teacherService;

    @Override
    public Teacher findById(final Long id) {
        return teacherService.findById(id).orElseThrow(()
                -> new NotFoundException(Constant.NOT_FOUND_RESOURCE_MESSAGE));
    }

    @Override
    public List<Teacher> findAll(final Pageable pageable) {
        return teacherService.findAll(pageable);
    }

    @Override
    public void save(final Teacher teacher) {
        teacherService.save(teacher);
    }

    @Override
    public void deleteById(final Long id) {
        teacherService.deleteById(id);
    }

    @Override
    public List<Teacher> findByParam(List<RequestParameter> params) {
        return teacherService.findByParam(params);
    }

    @Override
    public List<Teacher> findUpdatedRow(final LocalDateTime lastUpdateDate) {
        return teacherService.findUpdatedRow(lastUpdateDate);
    }

    @Autowired
    public void setTeacherService(final TeacherService teacherService) {
        this.teacherService = teacherService;
    }
}
