package by.gsu.math.db_service.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "housings")
@NoArgsConstructor
@AllArgsConstructor
public class Housing {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "address")
    private String address;

    @Size(min = 1, max = 55)
    @NotNull
    @Column(name = "housing_name", unique = true)
    private String name;

    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "housing")
    @JsonIgnore
    private List<Classroom> classrooms = new ArrayList<>();

    @Embedded
    @JsonIgnore
    private Audit audit = new Audit();
}
