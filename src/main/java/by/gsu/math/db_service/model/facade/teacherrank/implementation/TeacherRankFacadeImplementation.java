package by.gsu.math.db_service.model.facade.teacherrank.implementation;

import by.gsu.math.db_service.Constant;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.exception.NotFoundException;
import by.gsu.math.db_service.model.entity.TeacherRank;
import by.gsu.math.db_service.model.facade.teacherrank.TeacherRankFacade;
import by.gsu.math.db_service.service.teacherrank.TeacherRankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class TeacherRankFacadeImplementation implements TeacherRankFacade {

    private TeacherRankService teacherRankService;

    @Override
    public TeacherRank findById(final Long id) {
        return teacherRankService.findById(id).orElseThrow(()
                -> new NotFoundException(Constant.NOT_FOUND_RESOURCE_MESSAGE));
    }

    @Override
    public List<TeacherRank> findAll(final Pageable pageable) {
        return teacherRankService.findAll(pageable);
    }

    @Override
    public void save(final TeacherRank teacherRank) {
        teacherRankService.save(teacherRank);
    }

    @Override
    public void deleteById(final Long id) {
        teacherRankService.deleteById(id);
    }

    @Override
    public List<TeacherRank> findByParam(List<RequestParameter> params) {
        return teacherRankService.findByParam(params);
    }

    @Override
    public List<TeacherRank> findUpdatedRow(final LocalDateTime lastUpdateDate) {
        return teacherRankService.findUpdatedRow(lastUpdateDate);
    }

    @Autowired
    public void setTeacherRankService(final TeacherRankService teacherRankService) {
        this.teacherRankService = teacherRankService;
    }
}
