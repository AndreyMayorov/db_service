package by.gsu.math.db_service.model.facade.group.implementation;

import by.gsu.math.db_service.Constant;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.exception.NotFoundException;
import by.gsu.math.db_service.model.entity.StudyGroup;
import by.gsu.math.db_service.model.facade.group.StudyGroupFacade;
import by.gsu.math.db_service.service.group.StudyGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class StudyGroupFacadeImplementation implements StudyGroupFacade {

    private StudyGroupService studyGroupService;

    @Override
    public StudyGroup findById(final Long id) {
        return studyGroupService.findById(id).orElseThrow(()
                -> new NotFoundException(Constant.NOT_FOUND_RESOURCE_MESSAGE));
    }

    @Override
    public List<StudyGroup> findAll(final Pageable pageable) {
        return studyGroupService.findAll(pageable);
    }

    @Override
    public void save(final StudyGroup studyGroup) {
        studyGroupService.save(studyGroup);
    }

    @Override
    public void deleteById(final Long id) {
        studyGroupService.deleteById(id);
    }

    @Override
    public List<StudyGroup> findByParam(List<RequestParameter> params) {
        return studyGroupService.findByParam(params);
    }

    @Override
    public List<StudyGroup> findUpdatedRow(final LocalDateTime lastUpdateDate) {
        return studyGroupService.findUpdatedRow(lastUpdateDate);
    }

    @Autowired
    public void setStudyGroupService(final StudyGroupService studyGroupService) {
        this.studyGroupService = studyGroupService;
    }
}
