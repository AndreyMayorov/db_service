package by.gsu.math.db_service.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Entity
@NoArgsConstructor
@DiscriminatorValue(value = "Lesson")
public class Lesson extends StudyHour {

    @ManyToOne(targetEntity = Discipline.class)
    @NotNull
    private Discipline discipline;

    @ManyToOne(targetEntity = Teacher.class)
    @NotNull
    private Teacher teacher;

    @ManyToOne(targetEntity = Classroom.class)
    @NotNull
    private Classroom classroom;

    public Lesson(Long id, @NotNull StudyGroup studyGroup,
                  @NotNull LocalDate studyHourDate,
                  @NotNull LocalTime studyHourTime,
                  @NotNull Discipline discipline,
                  @NotNull Teacher teacher,
                  @NotNull Classroom classroom,
                  @NotNull Faculty faculty) {
        super(id, studyGroup, studyHourDate, studyHourTime, faculty);
        this.discipline = discipline;
        this.teacher = teacher;
        this.classroom = classroom;
    }
}
