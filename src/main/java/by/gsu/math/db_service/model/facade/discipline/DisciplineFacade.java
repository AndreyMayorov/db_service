package by.gsu.math.db_service.model.facade.discipline;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Discipline;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

public interface DisciplineFacade {

    Discipline findById(Long id);

    List<Discipline> findAll(Pageable pageable);

    void save(Discipline discipline);

    void deleteById(Long id);

    List<Discipline> findByParam(List<RequestParameter> params);

    List<Discipline> findUpdatedRow(LocalDateTime lastUpdateDate);
}
