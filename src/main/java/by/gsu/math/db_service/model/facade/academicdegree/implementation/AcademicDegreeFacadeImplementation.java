package by.gsu.math.db_service.model.facade.academicdegree.implementation;

import by.gsu.math.db_service.Constant;
import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.exception.NotFoundException;
import by.gsu.math.db_service.model.entity.AcademicDegree;
import by.gsu.math.db_service.model.facade.academicdegree.AcademicDegreeFacade;
import by.gsu.math.db_service.service.academicdegree.AcademicDegreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class AcademicDegreeFacadeImplementation implements AcademicDegreeFacade {

    private AcademicDegreeService academicDegreeService;

    @Override
    public AcademicDegree findById(final Long id) {
        return academicDegreeService.findById(id).orElseThrow(()
                -> new NotFoundException(Constant.NOT_FOUND_RESOURCE_MESSAGE));
    }

    @Override
    public List<AcademicDegree> findAll(final Pageable pageable) {
        return academicDegreeService.findAll(pageable);
    }

    @Override
    public void deleteById(final Long id) {
        academicDegreeService.deleteById(id);
    }

    @Override
    public void save(final AcademicDegree academicDegree) {
        academicDegreeService.save(academicDegree);
    }

    @Override
    public List<AcademicDegree> findByParam(List<RequestParameter> params) {
        return academicDegreeService.findByParam(params);
    }

    @Override
    public List<AcademicDegree> findUpdatedRow(final LocalDateTime lastUpdatedDate) {
        return academicDegreeService.findUpdatedRow(lastUpdatedDate);
    }

    @Autowired
    public void setAcademicDegreeService(final AcademicDegreeService academicDegreeService) {
        this.academicDegreeService = academicDegreeService;
    }
}
