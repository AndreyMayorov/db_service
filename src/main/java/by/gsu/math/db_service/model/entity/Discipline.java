package by.gsu.math.db_service.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Entity
@Table(name = "disciplines")
@NoArgsConstructor
@AllArgsConstructor
public class Discipline {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 55)
    @Column(name = "discipline_name", unique = true)
    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "discipline", targetEntity = Lesson.class)
    @JsonIgnore
    private List<StudyHour> lessons;

    @Embedded
    @JsonIgnore
    private Audit audit = new Audit();
}
