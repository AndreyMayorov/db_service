package by.gsu.math.db_service.model.facade.housing;

import by.gsu.math.db_service.dao.parameterizedsearch.RequestParameter;
import by.gsu.math.db_service.model.entity.Housing;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;

public interface HousingFacade {

    Housing findById(Long id);

    List<Housing> findAll(Pageable pageable);

    void save(Housing housing);

    void deleteById(Long id);

    List<Housing> findByParam(List<RequestParameter> params);

    List<Housing> findUpdatedRow(LocalDateTime lastUpdateDate);
}
